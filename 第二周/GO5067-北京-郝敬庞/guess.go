package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var resnum, gaussnum int
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	x := r.Intn(100)
	y := r.Intn(100)

	lucknum := rand.Intn(10000)
	fmt.Printf("请输入正确答案：%d + %d = ", x, y)
	fmt.Scanln(&resnum)
	if x+y == resnum {
		tta := rand.Intn(10)
		fmt.Printf("回答正确，获得%d次抽奖机会。", tta)

		for i := 1; i <= tta; i++ {
			fmt.Printf("请抽奖%d: ", i)
			fmt.Scanln(&gaussnum)
			if gaussnum == lucknum {
				fmt.Printf("恭喜您，中奖了！程序将退出。")
				break
			}
			fmt.Println("很遗憾，未中奖。")
		}
	} else {
		fmt.Println("回答错误，程序将退出。")
	}
}
