package main

import (
	"fmt"
	"strconv"
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	fmt.Printf("需要转换的值：%dB\n", bytesLength)
	//  bytesLength/1024可能是小数，所以要将int64的bytesLength 转为 float
	// Sprintf：格式化并返回一个字符串而不带任何输出
	const p int64 = 1024
	if bytesLength < p {
		resp = fmt.Sprintf("%.2fB", float64(bytesLength))
	} else if bytesLength >= p && bytesLength < (p*p) {
		resp = fmt.Sprintf("%.2fKB", float64(bytesLength/p))
	} else if bytesLength >= (p*p) && bytesLength < (p*p*p) {
		resp = fmt.Sprintf("%.2fMB", float64(bytesLength/(p*p)))
	} else if bytesLength >= (p*p*p) && bytesLength < (p*p*p*p)
		resp = fmt.Sprintf("%.2fGB", float64(bytesLength/(p*p*p)))
	} else if bytesLength >= (p*p*p*p) && bytesLength < (p*p*p*p*p) {
		resp = fmt.Sprintf("%.2fTB", float64(bytesLength/(p*p*p*p)))
	} else if bytesLength >= (p*p*p*p*p) {
		resp = fmt.Sprintf("%.2fEB", float64(bytesLength/(p*p*p*p*p)))
	}
	// 返回转换的结果
	return resp

}

// 注意下语法
