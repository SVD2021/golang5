/*
3做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
请回答如下问题, 比如: a + b =:  m
恭喜你 回答正确，随机获取n[1~10]的抽奖机会
当前是你第1次抽奖: 抽奖结果 未中奖
当前是你第2次抽奖. 抽奖结果 未中奖
当前是你第3次抽奖. 抽奖结果 未中奖
*/
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	var exitflag bool = false
	count := 0
	for {
		count++
		if exitflag == false {
			fmt.Print("欢迎使用抽卡小程序，请输入以下问题的答案，回车确认答案，输入 0 退出程序")
			//回答问题，判断回答问题的正确与否
			num1 := 1
			num2 := 2
			fmt.Printf("%d + %d = ?", num1, num2)
			var answer int
			fmt.Scanln(&answer) //读取控制台键盘输入的内容赋值给answer
			if answer == num1+num2 {
				rand.Seed(time.Now().UnixNano())
				n := rand.Intn(10) + 1   //随机获取n[1~10]的抽奖机会
				prob := rand.Intn(10000) //设置抽奖概率万分之一,随机生成一个0-9999的整数
				fmt.Printf("回答正确，你有%d 次抽奖机会", n)
				for i := 1; i <= n; i++ {
					rand.Seed(time.Now().UnixNano())
					luckynumber := rand.Intn(10000)
					if luckynumber == prob {
						fmt.Printf("当前是你第%d次抽奖，恭喜您，中奖了！\n", i)
						fmt.Printf("%d次十连中奖\n", count)
						break
					} else {
						fmt.Printf("当前是你第%d次抽奖: 抽奖结果 未中奖", i)
					}
				}
			} else if answer == 0 {
				exitflag = true
			} else {
				fmt.Println("很抱歉，回答错误！")
			}
		} else {
			//os.Exit(1)
			break
		}
	}
}

// 未实现目标效果
