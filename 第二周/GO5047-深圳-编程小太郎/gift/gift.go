package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var times int
	times++
	var input_num int
	rand.Seed(time.Now().UnixNano())
	current_num := rand.Intn(100)
	for {
		if times == 10 {
			fmt.Println("抽奖机会已用完！")
			break
		}
		fmt.Printf("请输入一个1~100的整数进行抽奖，有10次机会,当前是%d次\n", times)
		fmt.Scan(&input_num)
		if input_num == current_num {
			fmt.Printf("恭喜中奖！%d\n", current_num)
			fmt.Println("退出抽奖程序!")
			break
		} else if input_num < current_num {
			times++
			fmt.Println("太小了！没用中奖！")
		} else if input_num > current_num {
			times++
			fmt.Println("太大了！没用中奖！")
		} else {
			fmt.Printf("输入不是整数，请重新输入！\n")
			times = 0
		}
	}
}
