package main

import (
	"fmt"
	"strconv"
)

func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	// bytesLength
	var s string = "BKMGTE" //定义好的单位
	var q rune              //储存返回的单位
	fmt.Printf("需要转换的值: %dB\n", bytesLength)
	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	for _, v := range s {
		q = v
		resp = strconv.Itoa(int(0b1111111111&int(bytesLength))) + string(q) + " " + resp //保留低位数据成字符串
		//如果数值超过1024 右移10bit
		if bytesLength >= 1024 {
			//fmt.Println(strconv.Itoa(int(0b1111111111&int(bytesLength))) + string(q))
			bytesLength >>= 10
		} else {
			break
		}
	}
	// 返回转换的结果
	return resp
}
func main() {
	fmt.Println(HumanBytesLoaded(2132343215087632))
}

