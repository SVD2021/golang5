package main

import (
	"fmt"
	"strconv"
)

/*
写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成
	测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用
*/

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	// bytesLength 初始值单位: B

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	switch {
	case bytesLength >= (1 << 50):
		s, err := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/float64((1<<50))), 64)
		if err != nil {
			return "转换失败！"
		}
		return fmt.Sprintf("%.2fEB", s)
	case bytesLength >= (1 << 40):
		s, err := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/float64((1<<40))), 64)
		if err != nil {
			return "转换失败！"
		}
		return fmt.Sprintf("%.2fTB", s)
	case bytesLength >= (1 << 30):
		s, err := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/float64((1<<30))), 64)
		if err != nil {
			return "转换失败！"
		}
		return fmt.Sprintf("%.2fGB", s)
	case bytesLength >= (1 << 20):
		s, err := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/float64((1<<20))), 64)
		if err != nil {
			return "转换失败！"
		}
		return fmt.Sprintf("%.2fMB", s)
	case bytesLength >= (1 << 10):
		s, err := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/float64((1<<10))), 64)
		if err != nil {
			return "转换失败！"
		}
		return fmt.Sprintf("%.2fKB", s)
	default:
		return fmt.Sprintf("%dB", bytesLength)
	}
}

func main() {
	/*
		运行方式: go run demo00.go
	*/
	var value int64
	fmt.Print("请输入需要转换的值(请输入整数，否则计算会有偏差):")
	fmt.Scan(&value)
	v := HumanBytesLoaded(value)
	fmt.Printf("已转换的值: %s\n", v)
}
