package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	code := `1000 1000 0110 0011
0110 0111 0000 1101
0101 0101 1001 1100
0110 1011 0010 0010
0111 1010 0111 1111
0100 1110 0010 1101
0101 0110 1111 1101
0111 1110 1010 0010`

	for _, v := range strings.Split(code, "\n") {
		cl := strings.Replace(v, " ", "", -1)
		// 转换成二进制
		b, _ := strconv.ParseUint(cl, 2, 16)
		fmt.Printf("%c", b)
	}

}
