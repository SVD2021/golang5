package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix()) //(设置一个种子)
	x := rand.Intn(100)
	var Input int
	for i := 1; i <= 5; i++ {
		fmt.Print("请输入一个数字:")
		fmt.Scan(&Input)
		if Input > x {
			fmt.Println("猜的数字过大")
		} else {
			if Input < x {
				fmt.Println("猜的数字过小")
			} else {
				if Input == x {
					fmt.Print("恭喜您答对了")
					time.Sleep(time.Duration(1) * time.Second)
					break
				}
			}
		}
		if i == 5 {
			print("你真笨五次都没答对")
			time.Sleep(time.Duration(1) * time.Second)
			break
		}
	}
}

// 次数可以使用随机数，同时也需要满足规定的需求才进入抽奖逻辑
