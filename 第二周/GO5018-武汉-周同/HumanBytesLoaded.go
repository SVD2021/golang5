package main

import (
	"fmt"
	"math"
)

func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	fmt.Printf("需要转换的值： %dB ---->", bytesLength)
	if bytesLength >= int64(math.Pow(1024, 6)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 6))
		return resp + "EB"
	} else if bytesLength >= int64(math.Pow(1024, 5)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 5))
		return resp + "PB"
	} else if bytesLength >= int64(math.Pow(1024, 4)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 4))
		return resp + "TB"
	} else if bytesLength >= int64(math.Pow(1024, 3)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 3))
		return resp + "GB"
	} else if bytesLength >= int64(math.Pow(1024, 2)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 2))
		return resp + "MB"
	} else if bytesLength >= int64(math.Pow(1024, 1)) {
		resp = fmt.Sprintf("%.2f", float64(bytesLength)/math.Pow(1024, 1))
		return resp + "KB"
	} else {
		resp = fmt.Sprintf("%d", bytesLength)
		return resp + "B"
	}

}

func main() {
	fmt.Println(HumanBytesLoaded(1024))

}

// 参数可以考虑让用户输入
