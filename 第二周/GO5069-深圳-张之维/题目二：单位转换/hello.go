package main

import "fmt"

//a := "A"
func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	B := 1
	KB := B * 1024
	MB := KB * 1024
	GB := MB * 1024
	TB := GB * 1024
	EB := TB * 1024
	if bytesLength < 0 {
		resp = "请输入一个大于0的数"
	}
	if bytesLength >= int64(EB) {
		bytesLength /= int64(EB)
		resp = "EB"
	} else if bytesLength >= int64(TB) && bytesLength < int64(EB) {
		bytesLength /= int64(TB)
		resp = "TB"
	} else if bytesLength >= int64(GB) && bytesLength < int64(TB) {
		bytesLength /= int64(GB)
		resp = "GB"
	} else if bytesLength >= int64(MB) && bytesLength < int64(GB) {
		bytesLength /= int64(MB)
		resp = "MB"
	} else if bytesLength >= int64(KB) && bytesLength < int64(MB) {
		bytesLength /= int64(KB)
		resp = "KB"
	} else {
		resp = "B"
	}

	fmt.Printf("需要转换的值: %d", bytesLength)

	return resp
}
func main() {
	res := HumanBytesLoaded(2048000)
	fmt.Println(res)
}
