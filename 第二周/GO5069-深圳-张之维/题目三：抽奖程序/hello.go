package main

import (
	"fmt"
	"math/rand"
	"time"
)

func ChouJiang() {
	//用于生成随机的数
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(10)
	res := rand.Intn(10000)
	for i := 1; i <= n; i++ {
		var boom int
		fmt.Printf("您已获得%d次抽奖机会!请输入你的幸运数字(1~10000): ", n)
		fmt.Scan(&boom)
		if boom == res {
			fmt.Println("恭喜您！中奖了")
			break
		} else {
			fmt.Println("谢谢参与!")
		}
	}
}
func main() {
	rand.Seed(time.Now().UnixNano())
	a := rand.Intn(10)
	b := rand.Intn(10)
	var m int
	fmt.Printf("请答题：%d+%d\n", a, b)
	fmt.Scan(&m)
	if m == a+b {
		fmt.Println("回答正确,进入抽奖环节!")
		ChouJiang()
	} else {
		fmt.Println("回答错误，请重试!")
	}

}
