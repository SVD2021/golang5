package main

import (
	"fmt"
	"math/rand"
	"time"
)

func BintoChar() {
	slice := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	//遍历切片
	for i := 0; i < len(slice); i++ {
		fmt.Printf("二进制数为: %b\t转为unicode为: %U\t得到字符为:%c \n", slice[i], slice[i], slice[i])
	}
}

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	if bytesLength > 0 && bytesLength < 1024 {
		resp = fmt.Sprintf("resp单位转换后为:%dB", bytesLength)
	} else if bytesLength > 0 && bytesLength < 1024*1024 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fKB", float64(bytesLength)/float64(1024))
	} else if bytesLength > 0 && bytesLength < 1024*1024*1024 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fMB", float64(bytesLength)/float64(1024*1024))
	} else if bytesLength > 0 && bytesLength < 1024*1024*1024*1024 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fGB", float64(bytesLength)/float64(1024*1024*1024))
	} else if bytesLength > 0 && bytesLength < 1024*1024*1024*1024*1024 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fTB", float64(bytesLength)/float64(1024*1024*1024*1024))
	} else if bytesLength > 0 && bytesLength < 1024*1024*1024*1024*1024*1024 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fPB", float64(bytesLength)/float64(1024*1024*1024*1024*1024))
	} else if bytesLength > 0 && bytesLength <= 9223372036854775807 {
		resp = fmt.Sprintf("resp单位转换后为:%.2fEB", float64(bytesLength)/float64(1024*1024*1024*1024*1024*1024))
	} else {
		resp = "请输入大于0的bytesLength哦~"
	}
	// 返回转换的结果
	return resp
}

func LuckyDraw() {
	rand.Seed(time.Now().Unix())
	chance := rand.Intn(11)
	// fmt.Println(chance)
	if 0 == chance {
		fmt.Println("很遗憾您获得0次抽奖机会!欢迎惠顾！")
	} else {
		fmt.Printf("恭喜您回答正确!获得%d次抽奖机会!\n", chance)
		luckyNumber := 6666

		for i := 1; i <= chance; i++ {
			randomNumber := rand.Intn(10000)
			// fmt.Println(randomNumber)
			if luckyNumber == randomNumber {
				fmt.Printf("当前是您第%d次抽奖,恭喜您中奖了！\n", i)
			} else {
				fmt.Printf("当前是您第%d次抽奖,很遗憾未能中奖！\n", i)
			}
		}
	}

}

func main() {
	//第一题
	BintoChar()

	//第二题
	fmt.Println(HumanBytesLoaded(-1))
	fmt.Println(HumanBytesLoaded(0))
	fmt.Println(HumanBytesLoaded(102))
	fmt.Println(HumanBytesLoaded(1024))
	fmt.Println(HumanBytesLoaded(1024000))
	fmt.Println(HumanBytesLoaded(1024000000))
	fmt.Println(HumanBytesLoaded(1024000000000))
	fmt.Println(HumanBytesLoaded(1024000000000000))
	fmt.Println(HumanBytesLoaded(102400000000000000))
	fmt.Println(HumanBytesLoaded(9223372036854775807))

	//第三题
	m := 0
	fmt.Println("请输入2 + 2 = ?")
	fmt.Scanln(&m)
	fmt.Println(m)
	if 4 == m {
		fmt.Println("恭喜您回答正确!开始幸运抽奖吧~")
		LuckyDraw()
	} else {
		fmt.Println("回答错误，下次好运哦~")
	}
}
