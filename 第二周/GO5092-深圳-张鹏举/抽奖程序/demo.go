package main

import (
	"fmt"
	"math/rand"
	"time"
)

func lottery() {
	rand.Seed(time.Now().Unix())
	num := rand.Intn(1000)
	count := rand.Intn(10)
	tag := count
	for i := 1; i <= count; i++ {
		var input int
		fmt.Printf("你还有%d次抽奖机会,请输入你的幸运数字: ", tag)
		fmt.Scanln(&input)
		if input == num {
			fmt.Print("恭喜你!猜中啦")
			break
		} else {
			fmt.Println("猜错啦!加油哦~~")
		}

		tag -= 1
		
	}
	if tag == 0 {
		fmt.Printf("抱歉,本次抽奖没中,正确的数字为: %d", num)
	}
}

func main() {
	var (
		a int = 4
		b int = 5
		m int
	)

	fmt.Printf("请回答如下问题: %d + %d = ", a, b)

	fmt.Scanln(&m)

	if m == a+b {
		fmt.Println("恭喜你回答正确,即将进入抽奖环节....")
		lottery()
	} else {
		fmt.Println("回答错误")
	}

}

