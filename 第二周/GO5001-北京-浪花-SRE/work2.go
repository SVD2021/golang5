package main

import (
	"fmt"
	"strconv"

	"github.com/shopspring/decimal"
)

//写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成,
//测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用

func main() {

	HumanBytesLoaded(1024 * 1024 * 1024 * 1024 * 1550)
}

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %vB\n", bytesLength)

	kb, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/1024), 64)
	kbi := decimal.NewFromFloat(kb)
	fmt.Printf("转换结果为：%vKB\n", kbi)

	mb, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/1024/1024), 64)
	mbi := decimal.NewFromFloat(mb)
	fmt.Printf("转换结果为：%vMB\n", mbi)

	gb, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/1024/1024/1024), 64)
	gbi := decimal.NewFromFloat(gb)
	fmt.Printf("转换结果为：%vGB\n", gbi)

	tb, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/1024/1024/1024/1024), 64)
	tbi := decimal.NewFromFloat(tb)
	fmt.Printf("转换结果为：%vTB\n", tbi)

	eb, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", float64(bytesLength)/1024/1024/1024/1024/1024), 64)
	ebi := decimal.NewFromFloat(eb)
	fmt.Printf("转换结果为：%vEB\n", ebi)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB

	// 返回转换的结果
	return resp
}

// 可以使用当前学的内容实现下
