package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

//做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
//请回答如下问题, 比如: a + b =:  m
//恭喜你 回答正确，随机获取n[1~10]的抽奖机会
//当前是你第1次抽奖: 抽奖结果 未中奖
//当前是你第2次抽奖. 抽奖结果 未中奖
//当前是你第3次抽奖. 抽奖结果 未中奖
//...
func main() {
	count := 0
	for {
		count++
		//回答问题，判断回答问题的正确与否
		num1 := 1
		num2 := 2
		fmt.Printf("请回答如下问题：%d+%d=", num1, num2)
		var num3 int
		fmt.Scanln(&num3)
		if num3 == num1+num2 {
			//恭喜你 回答正确，随机获取n[1~10]的抽奖机会
			fmt.Println("恭喜你，回答正确！")
			rand.Seed(time.Now().UnixNano())         //随机种子，保证每次遍历获取不是重复的一些随机数据
			rn := fmt.Sprintf("%d", rand.Intn(10)+1) //获取随机整数0~10
			rni, err := strconv.Atoi(rn)

			if err != nil {
				panic(err)
			}
			base := 10000
			rbase := rand.Intn(base)
			for i := 1; i <= rni; i++ {
				rand.Seed(time.Now().UnixNano())
				rsbase := rand.Intn(base)
				if rsbase == rbase {
					fmt.Printf("当前是你第%d次抽奖，恭喜您，中奖了！\n", i)
					fmt.Printf("%d次十连终奖\n", count)
					os.Exit(1)
				} else {
					fmt.Printf("当前是你第%d次抽奖，抽奖结果：未中奖\n", i)
				}
			}
		} else {
			fmt.Println("很抱歉，回答错误！")
		}
	}
}

// 仅让回答了问题，并未有让用户抽奖，可以修改下
