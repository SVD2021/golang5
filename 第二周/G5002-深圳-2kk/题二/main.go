package main

import "fmt"

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB

	// 返回转换的结果
	return resp
}

func main() {
	HumanBytesLoaded()

}

// 注意下函数定义、调用、功能的实现
