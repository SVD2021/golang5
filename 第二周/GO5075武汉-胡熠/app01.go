package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func Test1() {
	/*
		二进制转成中文
	*/
	MySlice := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}

	for _, s := range MySlice {
		fmt.Printf("Unicode字符: %U 表示的字符是: %c\n", s, s)
	}

}

func ChouJiang() {
	/*
		抽奖
	*/
	var first_input int
	var second_input int

	rand.Seed(time.Now().UnixNano()) // 生成随机种子
	s1 := rand.Intn(10000)           // 中奖号码 随机数在1-10000内
	s2 := rand.Intn(10)              // 获取随机抽奖次数
	s3 := rand.Intn(10)              // 获取随机数
	s4 := rand.Intn(10)              // 获取随机数

	if s2 == 0 {
		s2 = 1
	}

	num := s3 + s4

	fmt.Printf("%d + %d = ?请在下一行输入结果: \n", s3, s4)
	fmt.Scanln(&first_input)

	if num == first_input {
		fmt.Printf("恭喜你 回答正确，恭喜你获得 %d 次抽奖机会; 请输入一个1-10000的数字开始抽奖: ", s2)
	} else {
		fmt.Println("no")
		os.Exit(0)
	}

	var j int = 1
	for {

	LOOP:
		stdin := bufio.NewReader(os.Stdin)
		line, _, err := stdin.ReadLine()
		if err != nil {
			fmt.Println("bufio.NewReader读取失败")
			// panic(err)
		}
		n, err := fmt.Sscanln(string(line), &second_input)

		if err != nil {
			fmt.Printf("fmt.Sscanln读取失败, [%#v] 输入错误 重新输入:  ", n)
			goto LOOP
			// fmt.Println()
			// panic(err)
		}

		if second_input != s1 {
			fmt.Println()
			fmt.Printf("输入的值是: %d  '[未抽中]' ", second_input)

		} else if second_input == s1 {
			fmt.Println("抽中")
			break
		}

		j++

		if j > s2 {
			fmt.Println()
			fmt.Println("\n抽奖次数上限 退出抽奖....")
			break

		} else {
			fmt.Printf("剩余%d 机会; 请在下一行输入抽奖号:\n ", (s2+1)-j)
		}
	}
	fmt.Printf("中奖号码为%d:", s1)
}

func load(s int64) string {
	/*
		单位转换 byte  换算成 k  m g
	*/
	suffix := ""
	b := s
	if s > (1 << 30) {
		suffix = "G"
		b = s / (1 << 30)
	} else if s > (1 << 20) {
		suffix = "M"
		b = s / (1 << 20)
	} else if s > (1 << 10) {
		suffix = "K"
		b = s / (1 << 10)
	} else {
		suffix = "byte"
	}
	return fmt.Sprintf("%d%s\n", b, suffix)
}

func main() {
	var value int64
	fmt.Scan(&value)
	com := load(value)
	fmt.Printf("%d byte == %s", value, com)

	ChouJiang()
	Test1()

}
