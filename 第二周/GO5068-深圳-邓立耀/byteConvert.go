package main

import (
	"fmt"
)

//字节转换运算
func HumanBytesLoaded(byteType int64) string {
	var nums string
	fmt.Printf("需要换算的值: %dB\n", byteType)
	if byteType < 1024 {
		nums = fmt.Sprintf("%3.2fB\n", float64(byteType)/float64(1))
	} else if byteType < (1024 * 1024) {
		nums = fmt.Sprintf("%3.2fKB\n", float64(byteType)/float64(1024))
	} else if byteType < (1024 * 1024 * 1024) {
		nums = fmt.Sprintf("%3.2fMB\n", float64(byteType)/float64(1024*1024))
	} else if byteType < (1024 * 1024 * 1024 * 1024) {
		nums = fmt.Sprintf("%3.2fGB\n", float64(byteType)/float64(1024*1024*1024))
	} else if byteType < (1024 * 1024 * 1024 * 1024 * 1024) {
		nums = fmt.Sprintf("%3.2fTB\n", float64(byteType)/float64(1024*1024*1024*1024))
	} else {
		nums = fmt.Sprintf("%3.2fEB\n", float64(byteType)/float64(1024*1024*1024*1024*1024))
	}

	return nums
}

func main() {
	fmt.Println("此程序为字节换算")

	fmt.Print("请输入你要换算的数字：")
	var nums int64
	fmt.Scan(&nums)
	fmt.Println("转换结果：", HumanBytesLoaded(nums))
}
