package main

import (
	"fmt"
	"strconv"
	// "reflect"
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	if bytesLength < 0 {
		resp = "Wrong Number!"
	} else if bytesLength < 1024 {
		resp = strconv.Itoa(int(bytesLength))
	} else if bytesLength < 1024*1024 {
		resp = strconv.FormatFloat(float64(bytesLength)/1024, 'f', 2, 64) + "KB"
	} else if bytesLength < 1024*1024*1024 {
		resp = strconv.FormatFloat(float64(bytesLength)/1024/1024, 'f', 2, 64) + "MB"
	} else if bytesLength < 1024*1024*1024*1024 {
		resp = strconv.FormatFloat(float64(bytesLength)/1024/1024/1024, 'f', 2, 64) + "GB"
	} else {
		resp = strconv.FormatFloat(float64(bytesLength)/1024/1024/1024/1024, 'f', 2, 64) + "TB"
	}
	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	// 返回转换的结果
	return resp
}

func main() {
	tr := HumanBytesLoaded(102488888888)
	fmt.Printf("转换后的值: %s", tr)
}

// 注意下换行，参数有由户输入试下
