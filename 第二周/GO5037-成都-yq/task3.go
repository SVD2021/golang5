package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	for {
		seconds := time.Now().Unix()
		rand.Seed(seconds)
		a := rand.Intn(100)
		b := rand.Intn(100)
		m := a + b
		var n int
		const winningNumber int = 5
		fmt.Printf("%d + %d =?, 回答正确可以获得抽奖机会:", a, b)
		fmt.Scanln(&n)
		if n != m {
			fmt.Println("很遗憾，回答错误")
			continue
		} else {
			chance := rand.Intn(10) + 1
			fmt.Printf("恭喜你 回答正确，随机获取%d的抽奖机会\n", chance)
			for i := 1; i <= chance; i++ {
				var r string
				n := rand.Intn(10000)
				if n == winningNumber {
					r = "中奖"
				} else {
					r = "未中奖"
				}
				fmt.Printf("当前是你第%d次抽奖: 抽奖结果 %s\n", i, r)
			}
		}
	}

}

// 抽奖逻辑未实现
