package cvEncoding

import (
	"fmt"
	"strings"
)

// 二进制赋值
func Binary() {
	// 二进制 0B或者0b 表示
	var bin1 int = 0b1101
	fmt.Printf("%b的十进制为%d \n", bin1, bin1)
}

// 八进制
func Octal() {
	// 八进制 0O或者0o 表示
	var num1 int = 0o1234567
	fmt.Printf("%o 的十进制为 %d \n", num1, num1)
}

// 十六进制
func Hexadecimal() {
	// 十六进制 0X或者0x 表示
	var num1 int = 0x1234
	var num2 int = 0xf
	fmt.Printf("%x 的十进制为%d \n", num1, num1)
	fmt.Printf("%X 的十进制为%d \n", num2, num2)

}

// 对slice 反转
func reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}

// N的N次方函数
func ntNpower(x int, n int) int {
	ans := 1
	for ; n > 0; n-- {
		ans *= x

	}
	return ans
}

// 二进制转换十进制函数
func BcvD(Strs []string) []int {
	ints := []int{}
	for _, v := range Strs {
		s := strings.Split(v, "")
		s = reverse(s)
		var n int
		for k, v := range s {
			if v == "1" {
				n += ntNpower(2, k)
			}
		}
		ints = append(ints, n)
	}
	return ints
}

// 请将这段二进制翻译成中文(unicode编码)

// 1000 1000 0110 0011
// 0110 0111 0000 1101
// 0101 0101 1001 1100
// 0110 1011 0010 0010
// 0111 1010 0111 1111
// 0100 1110 0010 1101
// 0101 0110 1111 1101
// 0111 1110 1010 0010

func conversion() {
	ints := []int{
		0B1000100001100011,
		0B0110011100001101,
		0B0101010110011100,
		0B0110101100100010,
		0B0111101001111111,
		0B0100111000101101,
		0B0101011011111101,
		0B0111111010100010,
	}

	for _, v := range ints {
		fmt.Printf("二进制: %b  八进制: %o 十进制: %d 十六进制: %X  Unicode编码: %U Unicode对应字符: %c \n", v, v, v, v, v, v)
	}
}

// 仅把作业实现即可，不要把上课的记录也放在作业里
