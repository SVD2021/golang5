package main

import (
	"fmt"
	"math/rand"
	"time"
)

func GetRandNum(RangeNum int) (LuckyNum int) {
	rand.Seed(time.Now().UnixNano())
	LuckyNum = rand.Intn(RangeNum)
	return LuckyNum
}

func main() {
	const (
		Retry       = 5
		RangeNumber = 10
	)
	LuckyNum := GetRandNum(RangeNumber)
	fmt.Printf("抽奖程序，%d次机会，数字范围:%d\n", Retry, RangeNumber)
	for i := 1; i <= Retry; i++ {
		fmt.Printf("第%d次机会\n", i)
		var InputNum int
		fmt.Scan(&InputNum)
		if InputNum == LuckyNum {
			fmt.Print("恭喜你猜中了， ")
			break
		}
	}
	fmt.Printf("幸运数字是：%d\n", LuckyNum)
}
