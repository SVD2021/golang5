package main

import "fmt"

func main() {
	// 参考 GO5028-新疆-编程蔡徐坤
	BinCode := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	for _, v := range BinCode {
		fmt.Printf("二进制: %b，Unicode：%U，字符: %c\n", v, v, v)
	}
	// 	fmt.Printf("打印二进制内容 %U\n", BinCode)
	// a := 5
	// fmt.Printf("二进制: %b\n", a)
	// // b := 101
	// fmt.Printf("十进制：%d\n", a)
}
