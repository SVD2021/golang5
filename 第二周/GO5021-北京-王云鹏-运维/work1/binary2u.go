package main

import "fmt"

func main() {
	binary := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	for _, v := range binary {
		fmt.Printf("二进制:%b, unicode:%U, 字符:%c\n", v, v, v)
	}
}

// 完成的不错
