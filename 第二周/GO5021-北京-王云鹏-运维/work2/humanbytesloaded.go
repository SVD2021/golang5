package main

import "fmt"

func main() {
	res := HumanBytesLoaded(102400000099918)
	fmt.Println(res)
}

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	if bytesLength < 1024 {
		resp = fmt.Sprintf("%.2fB", float64(bytesLength)/float64(1))
	} else if bytesLength >= 1024 && bytesLength < (1024*1024) {
		resp = fmt.Sprintf("%.2fKB", float64(bytesLength)/float64(1024))
	} else if bytesLength >= (1024*1024) && bytesLength < (1024*1024*1024) {
		resp = fmt.Sprintf("%.2fMB", float64(bytesLength)/float64(1024*1024))
	} else if bytesLength >= (1024*1024*1024) && bytesLength < (1024*1024*1024*1024) {
		resp = fmt.Sprintf("%.2fGB", float64(bytesLength)/float64(1024*1024*1024))
	} else if bytesLength >= (1024*1024*1024*1024) && bytesLength < (1024*1024*1024*1024*1024) {
		resp = fmt.Sprintf("%.2fTB", float64(bytesLength)/float64(1024*1024*1024*1024))
	} else if bytesLength >= (1024 * 1024 * 1024 * 1024 * 1024) {
		resp = fmt.Sprintf("%.2fEB", float64(bytesLength)/float64(1024*1024*1024*1024*1024))
	}
	// 返回转换的结果
	return resp
}

// 参数可以考虑让用户输入
