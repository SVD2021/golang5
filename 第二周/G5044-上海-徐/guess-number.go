package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func GetNum() (nItem int, input int) {
	var inputNum int
	stdin := bufio.NewReader(os.Stdin)

	line, _, err := stdin.ReadLine()
	if err != nil {
		panic(err)
	}

	n, err := fmt.Sscanln(string(line), &inputNum)
	if err != nil {
		panic(err)
	}

	return n, inputNum
}

func main() {
	var (
		initNum int = 100
		num     int
	)

	rand.Seed(time.Now().UnixNano()) //函数是用来创建随机数的种子,如果不执行该步骤创建的随机数是一样的，因为默认Go会使用一个固定常量值来作为随机种子
	num = rand.Intn(initNum)

	fmt.Println("猜数字:")

	for i := 1; i <= 10; i++ {
		_, inputNum := GetNum()

		if inputNum == num {
			fmt.Println("恭喜你猜对了")
			fmt.Printf("猜了%d次", i)
			fmt.Println()
			fmt.Println(num)
			break
		} else if inputNum < num {
			fmt.Println("你猜错了,数字小了点")

		} else if inputNum > num {
			fmt.Println("你猜错了,数字大了点")
		}

		if i == 10 {
			fmt.Printf("很遗憾，%d次机会已经用完，请继续充值", i)
		}
	}
}
//  功能实现的不错