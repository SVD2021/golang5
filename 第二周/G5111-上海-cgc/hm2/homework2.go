package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
const (
	bu = 1 << (10 * (iota + 1))
	kb
	mb
	gb
	tb
	eb
)
*/
func main() {
	Start()
	//fmt.Println(bu,kb,mb,gb,tb,eb)
	//fmt.Println(HumanBytesLoaded(10240000000000))
}

/*
func HumanBytesLoaded(bytesLength int64) string {
	switch {
	case bytesLength < kb:
		return fmt.Sprintf("%dB=%.2fKB", bytesLength, float64(bytesLength)/float64(bu))
	case bytesLength < mb:
		return fmt.Sprintf("%dB=%.2fMB", bytesLength, float64(bytesLength)/float64(kb))
	case bytesLength < gb:
		return fmt.Sprintf("%dB=%.2fGB", bytesLength, float64(bytesLength)/float64(mb))
	case bytesLength < tb:
		return fmt.Sprintf("%dB=%.2fTB", bytesLength, float64(bytesLength)/float64(gb))
	case bytesLength < eb:
		return fmt.Sprintf("%dB=%.2fEB", bytesLength, float64(bytesLength)/float64(tb))

	default:
		return fmt.Sprintln("%dB=%dB", bytesLength, bytesLength)
	}

}
*/

func Start() {
	rand.Seed(time.Now().Unix())
	anser := -1
	for {
		if anser != -1 {
			continue
		}
		m, n := rand.Intn(10), rand.Intn(10)
		fmt.Printf("请回答如下问题：%d+%d=", m, n)
		fmt.Scanf("%d\n", &anser)
		if anser != m+n {
			fmt.Println("很遗憾，回答错误")
			return
		}
		fmt.Println("恭喜你回答正确，进入抽奖")
		if duang() {
			return
		}
		fmt.Println("就差一点，再试试吧！")
		anser = -1

	}
}

var (
	rollCount  = 100
	totalCount = 0
	lucky      = rand.Intn(rollCount)
)

func duang() bool {
	count := rand.Intn(10) + 1
	totalCount := 0
	fmt.Printf("你当前有[%d]次抽奖机会\n", count)
	for i := 1; i <= count; i++ {
		ticket := rand.Intn(rollCount)
		totalCount++
		if ticket == lucky {
			fmt.Printf("当前是你第%d次抽奖：抽奖结果【中奖】。幸运数字：%d，当前数字：%d\n", totalCount, lucky, ticket)
		}
		fmt.Printf("当前是你第%d次抽奖：抽奖结果【未中奖】。幸运数字：%d，当前数字：%d\n", totalCount, lucky, ticket)

	}
	return false
}
