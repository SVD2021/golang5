package main

import "fmt"

func main() {
	/*	m := 0.01
		for i := 1; i <= 100; i++ {
			m++
		}
		fmt.Println(m)
	*/
	const (
		a = 1 << iota 
		b
		c
		d
	)
	fmt.Println(a, b, c, d)
}
