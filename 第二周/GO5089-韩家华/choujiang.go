package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var (
		a int
		c string
	)
	fmt.Println("请输入1-10000幸运数字")
	fmt.Scan(&a)

	fmt.Printf("请确认您输入的幸运数字是否为%d\n", a)
	fmt.Println("确认输入yes，打错了请按no")
	fmt.Scan(&c)

	for i := 3; i > 0; i-- {
		if c == "yes" {
			break
		} else {
			fmt.Printf("您还有%d此机会重新输入\n", i)
			fmt.Println("请输入幸运数字")
			fmt.Scan(&a)
			fmt.Printf("请确认您输入的幸运数字是否为%d\n", a)
			fmt.Println("确认输入yes，打错了请按no")
			fmt.Scan(&c)
		}
	}
	if c == "no" {
		return
	}
	rand.Seed(time.Now().UnixNano())
	count1 := rand.Intn(10000)

	if &a != &count1 {
		fmt.Printf("您得幸运数字为%d\n", a)
		fmt.Printf("中奖数字为%d\n", count1)
		fmt.Print("很遗憾你没中奖")
	} else {
		fmt.Println("恭喜你第次抽奖. 抽奖结果 中奖！！")
	}
}
