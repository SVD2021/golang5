package main

import (
	"fmt"
	"math/rand"
	"time"
)

func luckDraw() {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(10)
	n2 := rand.Intn(10000)

	fmt.Printf("回答正确，随机获取 [1~10] 的抽奖机会: %v 次\n", n)
	for i := 1; i <= n; i++ {
		res := 0
		fmt.Println("请输入需要抽奖的数字:")
		fmt.Scan(&res)
		if res == n2 {
			fmt.Println("恭喜抽奖成功")
			break
		} else {
			fmt.Printf("当前是你第%v次抽奖: 抽奖结果 未中奖  \n", i)
		}
	}

}

func main() {
	a := 1
	b := 2
	m := 0
	fmt.Println("请输入 1 + 2 = ?")
	fmt.Scanln(&m)
	if a+b == m {
		luckDraw()
	} else {
		fmt.Println("输入错误")
	}
}

// 可以在结束时输出正确结果
