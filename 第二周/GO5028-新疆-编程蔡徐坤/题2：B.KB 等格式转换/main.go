package main

import (
	"fmt"
)

func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	B := 1
	KB := B * 1024
	MB := KB * 1024
	GB := MB * 1024
	TB := GB * 1024
	EB := TB * 1024

	if bytesLength <= 0 || bytesLength > int64(EB) {
		resp = "输入的值错误"
	}

	if bytesLength >= int64(EB) {
		resp = "EB"
	} else if bytesLength < int64(EB) && bytesLength >= int64(TB) {
		resp = "TB"
	} else if bytesLength < int64(TB) && bytesLength >= int64(GB) {
		resp = "GB"
	} else if bytesLength < int64(GB) && bytesLength >= int64(MB) {
		resp = "MB"
	} else if bytesLength < int64(MB) && bytesLength >= int64(KB) {
		resp = "KB"
	} else {
		resp = "B"
	}

	// if bytesLength

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB

	// 返回转换的结果
	return resp
}

func main() {
	res := HumanBytesLoaded(1023)
	fmt.Println(res)
}

// 函数的调用、功能实现，留意下return
