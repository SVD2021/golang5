package main

import "fmt"

/*
2. 写一个单位转换的函数, 同学只需要把函数逻辑部分编写完成,

测试方法: main 函数内部 通过HumanBytesLoaded(1024)调用

```go
// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
    var resp string

    // bytesLength
    fmt.Printf("需要转换的值: %dB", bytesLength)

    // 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB

    // 返回转换的结果
    return resp
}
```
*/

func HumanBytesLoaded(bytesLength int64) string {
	var resp string
	// 定义转换关系
	B := int64(1)
	KB := 1024 * B
	MB := 1024 * KB
	GB := 1024 * MB
	TB := 1024 * GB
	PB := 1024 * TB
	EB := 1024 * PB

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	if bytesLength < 1024 {
		resp = fmt.Sprintf("%.2fB", float64(bytesLength)/float64(B))
	} else if bytesLength >= KB && bytesLength < MB {
		resp = fmt.Sprintf("%.2fKB", float64(bytesLength)/float64(KB))
	} else if bytesLength >= MB && bytesLength < GB {
		resp = fmt.Sprintf("%.2fMB", float64(bytesLength)/float64(MB))
	} else if bytesLength >= GB && bytesLength < TB {
		resp = fmt.Sprintf("%.2fGB", float64(bytesLength)/float64(GB))
	} else if bytesLength >= TB && bytesLength < PB {
		resp = fmt.Sprintf("%.2fTB", float64(bytesLength)/float64(TB))
	} else if bytesLength >= PB && bytesLength < EB {
		resp = fmt.Sprintf("%.2fPB", float64(bytesLength)/float64(PB))
	} else if bytesLength >= PB {
		resp = fmt.Sprintf("%.2fEB", float64(bytesLength)/float64(EB))
	}
	// 返回转换的结果
	return resp
}

func main() {
	res := HumanBytesLoaded(2097152)
	fmt.Println(res)
}
