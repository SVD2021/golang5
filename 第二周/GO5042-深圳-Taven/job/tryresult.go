package main

import (
	"fmt"
	"math/rand"
	"time"
)

func lottery(res int64) {
	n := 0
	rand.Seed(time.Now().UnixNano())
	for n < 10 {
		for i := 1; i <= 10; i++ {
			num := rand.Int63n(1000)
			if num != res {
				fmt.Printf("当前是你第%d轮 第%d次抽奖 抽奖数字为: %d  未中奖\n", n, i, num)
			} else {
				fmt.Printf("当前是你第%d轮 第%d次抽奖 抽奖数字为: %d  恭喜中奖了\n", n, i, num)
				return
			}
		}
		n++
	}

}
func main() {
	var result int64
	// 中奖号码
	win := 101
	fmt.Printf("请回答如下问题\n1+6=？\n")
	_, err := fmt.Scan(&result)
	if err != nil {
		fmt.Println(err)
		return
	}
	if result == 7 {
		lottery(int64(win))
	}

}

//未实现抽奖逻辑
