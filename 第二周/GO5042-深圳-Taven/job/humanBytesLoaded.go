package main

import "fmt"

func humanBytesLoaded(n int64) string {

	if n < 1024 {
		return fmt.Sprintf("%2.fB", float64(n))
	}
	if n < 1024*1024 {
		return fmt.Sprintf("%.2fKB", float64(n)/float64(1024))
	}
	if n < 1024*1024 {
		return fmt.Sprintf("%.2fMB", float64(n)/float64(1024*1024))
	}
	if n < 1024*1024*1024*1024 {
		return fmt.Sprintf("%.2fGB", float64(n)/float64(1024*1024*1024))
	}
	if n < 1024*1024*1024*1024*1024 {
		return fmt.Sprintf("%.2fTB", float64(n)/float64(1024*1024*1024*1024))
	}
	return fmt.Sprintf("%.2fEB", float64(n)/float64(1024*1024*1024*1024*1024))

}

func main() {
	for {
		var size int64
		fmt.Printf("输入Size: ")
		_, err := fmt.Scan(&size)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(n)
		fmt.Printf("换算为：%s\n", humanBytesLoaded(size))

	}

}

// 完成的不错，考虑下中断即可
