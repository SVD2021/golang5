package main

/**
 * @Classname demo06
 * @Description TODO
 * @author cjf
 * @Date 2021/6/5 17:19
 * @Version V1.0
 */

import "fmt"

//标签与跳转

func main() {
LOOP:
	for i := 1; i <= 10; i++ {

		fmt.Print("LOOP")
		goto LOOP
		//死循环
	}
}
