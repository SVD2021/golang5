package main

import (
	"fmt"
	"strconv"
)

/**
 * @Classname demo06
 * @Description TODO
 * @author cjf
 * @Date 2021/6/5 17:19
 * @Version V1.0
 */

//数据类型转换
func main() {
	a := 3.14
	b := int(a)
	c := "hello"
	fmt.Print(a, "\n")
	fmt.Print(b, "\n")
	fmt.Print(c)

	fmt.Print("=============================")

	k := Age(10)
	var v int = 12
	fmt.Println(k, v)

	v = int(k)

	//strconv int与string类型转换

	str := strconv.Itoa(100)
	fmt.Print(str)

	i := 3.14
	var j string
	j = strconv.Itoa(int(i))
	fmt.Print(j)

	ic := "abc"
	var jc int
	var err error
	jc, _ = strconv.Atoi(ic)
	fmt.Print(jc, err)

	//String转换成其他的类型

	//进制之间转换  二进制和十进制
}

type Age int
