package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//模拟抽卡活动、80连抽必中奖
	//将时间戳设置成种子数
	rand.Seed(time.Now().UnixNano())
	ticket := rand.Intn(1000)
	fmt.Printf("本期中奖号码为：%d\n", ticket)
	start := 1
	var count int
	for {
		// ticket2 := rand.Intn(10000)
		fmt.Println("请输入抽奖次数:")
		fmt.Scan(&count)

		switch count {
		case 1:
			ticket2 := rand.Intn(1000)
			if ticket2 == ticket {
				fmt.Printf("抽奖结果%d恭喜您，中奖了。\n", ticket2)
				return
			} else {
				fmt.Printf("当前是您第%d抽奖，抽奖结果%d：未中奖。\n", start, ticket2)
				start += 1
			}
		case 10:
			for n := 1; n <= count; n++ {
				ticket2 := rand.Intn(10000)
				if ticket2 == ticket {
					fmt.Printf("抽奖结果%d恭喜您，中奖了。\n", ticket2)
					return
				} else {
					fmt.Printf("当前是您第%d抽奖，抽奖结果%d：未中奖。\n", start, ticket2)
					start += 1
				}
			}
		default:
			fmt.Println("输入抽奖次数有误，请重新输入")
		}
		if start >= 80 {
			fmt.Println("恭喜您，中奖了。")
			break
		}

	}
}
