package main

import (
	"fmt"
	"strconv"
)

// HumanBytesLoaded 单位转换(B KB MB GB TB EB)
func HumanBytesLoaded(bytesLength int64) string {
	var resp string

	// bytesLength
	fmt.Printf("需要转换的值: %dB\n", bytesLength)

	// 单位转换的逻辑 将 bytesLength ==> resp, 比如 1024 ==> 1KB
	switch {
	case bytesLength < 1024:
		resp = strconv.FormatInt(bytesLength, 10) + "B"
	case bytesLength < (1024 * 1024):
		resp = strconv.FormatFloat(float64(bytesLength)/float64(1024), 'f', 2, 64) + "KB"
	case bytesLength < (1024 * 1024 * 1024):
		resp = strconv.FormatFloat(float64(bytesLength)/float64(1024*1024), 'f', 2, 64) + "KB"
	case bytesLength < (1024 * 1024 * 1024 * 1024):
		resp = strconv.FormatFloat(float64(bytesLength)/float64(1024*1024*1024), 'f', 2, 64) + "GB"
	case bytesLength < (1024 * 1024 * 1024 * 1024 * 1024):
		resp = strconv.FormatFloat(float64(bytesLength)/float64(1024*1024*1024*1024), 'f', 2, 64) + "TB"
	case bytesLength < (1024 * 1024 * 1024 * 1024 * 1024 * 1024):
		resp = strconv.FormatFloat(float64(bytesLength)/float64(1024*1024*1024*1024*1024), 'f', 2, 64) + "EB"
	default:
		resp = string(bytesLength) + "长度过长不支持转换!!"
	}

	// 返回转换的结果
	return resp
}

func main() {
	var a int64
	fmt.Print("请输入需要转换的大小:")
	fmt.Scan(&a)
	res := HumanBytesLoaded(a)
	fmt.Printf("结果为：%v\n", res)
}
