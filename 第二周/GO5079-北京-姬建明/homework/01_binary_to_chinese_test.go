package homework

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestBinaryToChinese(t *testing.T) {
	/*
		请将这段二进制翻译成中文(unicode编码)
		1000 1000 0110 0011
		0110 0111 0000 1101
		0101 0101 1001 1100
		0110 1011 0010 0010
		0111 1010 0111 1111
		0100 1110 0010 1101
		0101 0110 1111 1101
		0111 1110 1010 0010
	*/
	// 将这些二进制手动添加0b
	var binary []int = []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	for _, s := range binary {
		fmt.Printf("%c\t", s)
	}
	fmt.Println()
}

// 升级版利用字符串切割和拼接
func TestBinaryToChineseV2(t *testing.T) {
	/*
		请将这段二进制翻译成中文(unicode编码)
		1000 1000 0110 0011
		0110 0111 0000 1101
		0101 0101 1001 1100
		0110 1011 0010 0010
		0111 1010 0111 1111
		0100 1110 0010 1101
		0101 0110 1111 1101
		0111 1110 1010 0010
	*/
	// 将这些二进制手动添加0b
	var s string = `1000 1000 0110 0011
	0110 0111 0000 1101
	0101 0101 1001 1100
	0110 1011 0010 0010
	0111 1010 0111 1111
	0100 1110 0010 1101
	0101 0110 1111 1101
	0111 1110 1010 0010`
	s1 := strings.ReplaceAll(s, " ", "") // 去除字符串中空格
	s2 := strings.Split(s1, "\n")        //按照换行符切割生成一个[]string
	// fmt.Printf("%T\n", s2)
	for _, s3 := range s2 {
		s4 := strings.TrimSpace(s3) //去除字符串首位空格
		// fmt.Printf("%s\n", s4)
		// s5, err := strconv.ParseUint(s4, 2, 16)
		s5, _ := strconv.ParseUint(s4, 2, 16)
		// 有个疑惑我用strconv.ParseInt转换二进制的时候会报错：32767 strconv.ParseInt: parsing "1000100001100011": value out of range
		// 用strconv.ParseUint就不会报错
		// fmt.Println(s5) //err 32767 strconv.ParseInt: parsing "1000100001100011": value out of range
		// fmt.Printf("")
		fmt.Printf("%c\t", s5)
	}
	fmt.Println()
}
