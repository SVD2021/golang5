package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

//将这段二进制翻译成中文(unicode编码),并打印出翻译过程(比如 二进制: 1000 1000 0110 0011, unicode: U+8863, 字符: 衣)
// 1000 1000 0110 0011
// 0110 0111 0000 1101
// 0101 0101 1001 1100
// 0110 1011 0010 0010
// 0111 1010 0111 1111
// 0100 1110 0010 1101
// 0101 0110 1111 1101
// 0111 1110 1010 0010

//切片方式解决
func slice()  {
	 slice := []int{
		0b1000100001100011,
		0b0110011100001101,
		0b0101010110011100,
		0b0110101100100010,
		0b0111101001111111,
		0b0100111000101101,
		0b0101011011111101,
		0b0111111010100010,
	}
	for _, v :=range slice{
		fmt.Printf("二进制:%b, unicode:%U, 字符:%c\n", v, v, v)
	}
}
//"产品"一：支持用户从控制台标准输入逐行解析
func bin_unicode_1() {
	var bin_arrys string
	i := 1
	for {
		//用户从控制台输入源二进制字符串块
		fmt.Printf("请输入第%d组二进制数字(输入'exit'结束)：\n", i)
		user_input := bufio.NewReader(os.Stdin)
		line, _, err := user_input.ReadLine()
		if err != nil {
			panic(err)
		}
		bin_arrys = string(line)
		//for循环退出机制
		if bin_arrys == "exit" {
			break
		}
		//字符串去"//"
		bin_arrys = strings.Replace(bin_arrys, "//", "", -1)
		//字符串去空格
		bin_arrys = strings.ReplaceAll(bin_arrys, " ", "")
		//字符串转化为二进制
		bin_arrys_0b, err := strconv.ParseUint(bin_arrys, 2, 64)
		if err != nil {
			panic(err)
		}
		fmt.Printf("二进制:%b, unicode:%U, 字符:%c\n", bin_arrys_0b, bin_arrys_0b, bin_arrys_0b)
		i++
	}

}

//"产品"二：支持对文件中二进制整体解析
func bin_unicode_2() {
	var bin_arrys string
	//从文件中逐行读取源二进制字符串块
	file, err := os.Open("../bin.list") //请更改为文件的真实路径
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		bin_arrys = scanner.Text()
		//字符串去"//"
		bin_arrys = strings.Replace(bin_arrys, "//", "", -1)

		//字符串去空格
		bin_arrys = strings.ReplaceAll(bin_arrys, " ", "")

		//字符串转化为二进制

		bin_arrys_0b, err := strconv.ParseUint(bin_arrys, 2, 64)
		if err != nil {
			panic(err)
		}
		fmt.Printf("二进制:%b, unicode:%U, 字符:%c\n", bin_arrys_0b, bin_arrys_0b, bin_arrys_0b)

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	slice()
	bin_unicode_1()
	bin_unicode_2()

}
