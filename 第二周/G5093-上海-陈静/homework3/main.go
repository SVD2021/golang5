package main

import (
	"fmt"
	"math/rand"
	"time"
)

func luckDraw(n int, x int) {
	n2 := rand.Intn(10000)
	for i := 1; i <= n; i++ {
		if x == n2 {
			fmt.Println("恭喜抽奖成功")
			break
		} else {
			fmt.Printf("当前是你第%d次抽奖: 抽奖结果 未中奖  \n", i)
		}
	}

}

func main() {
	var m int
	var x int
	fmt.Println("请输入 1 + 2 = ?")
	fmt.Scanln(&m)
	if 1+2 == m {
		rand.Seed(time.Now().UnixNano())
		n := rand.Intn(10)
		fmt.Printf("回答正确，随机获取 [1~10] 的抽奖机会: %d 次\n", n)
		fmt.Println("请输入需要抽奖的数字:")
		fmt.Scan(&x)
		luckDraw(n, x)
	} else {
		fmt.Println("输入错误")
	}
}

// 仅对结果进行了一次判断，可以修改下
