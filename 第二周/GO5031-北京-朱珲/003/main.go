package main

import (
	"fmt"
	"math/rand"
	"time"
)

/*
做一个抽卡程序, 中奖率万分之一, 看看你多少个10连能中奖
请回答如下问题, 比如: a + b =:  m
恭喜你 回答正确，随机获取n[1~10]的抽奖机会
当前是你第1次抽奖: 抽奖结果 未中奖
当前是你第2次抽奖. 抽奖结果 未中奖
当前是你第3次抽奖. 抽奖结果 未中奖
...
*/
func main() {
	var a, b int = 3, 4
	var c int
	fmt.Printf("请输入 %d + %d = ?\n", a, b)
	fmt.Scanln(&c)
	if c == a+b {
		start()
	} else {
		fmt.Println("回答错误")
	}
}

func start() {
	rand.Seed(time.Now().Unix())
	a := rand.Intn(10)
	b := rand.Intn(10000)
	fmt.Printf("获取%d次抽奖机会\n", a)
	for i := 1; i <= a; i++ {
		var r int
		fmt.Printf("请输入数字\n")
		fmt.Scan(&r)
		if r == b {
			fmt.Println("恭喜中奖")
			break
		} else {
			fmt.Printf("%d次未中奖\n", i)
		}
	}
}

// 可以加上提示
