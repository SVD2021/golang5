package main

import (
	"container/list"
	"sync"
)

//实现一个链式栈(底层使用存储使用链表实现)
// type Stack struct {
// 	store List
// }
//首先，定义一个栈的结构体
type Stack struct {
	list *list.List
	lock *sync.RWMutex
}

//初始化一个栈
func NewStack() *Stack {
	list := list.New()
	lock := &sync.RWMutex{}
	return &Stack{list, lock}
}

//构建一个栈的结构体push方法
func (s *Stack) Push(value interface{}) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.list.PushBack(value)
}

//构建一个栈的结构体pop方法
func (s *Stack) Pop() interface{} {
	s.lock.Lock()
	defer s.lock.Unlock()
	tail := s.list.Back()
	if tail == nil {
		return nil
	} else {
		s.list.Remove(tail)
		return tail.Value
	}

}

//构建一个栈的查看最上面元素的peak方法
func (s *Stack) Peak() interface{} {
	s.lock.RLock()
	defer s.lock.RUnlock()
	tail := s.list.Back()
	if tail == nil {
		return nil
	} else {
		return tail.Value
	}
}
//构建一个栈的长度的len方法
func (s *Stack)Len()int { 
 s.lock.RLock()
 defer s.lock.RUnlock()
 return s.list.Len()	
}
