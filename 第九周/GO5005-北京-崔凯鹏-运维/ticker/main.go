package main

import (
	"container/heap"
	"fmt"
	"ticker01/timeheap"
	"time"
)

func main() {
	d1 := timeheap.NewDateHeap(30, "30")
	d2 := timeheap.NewDateHeap(40, "40")
	d3 := timeheap.NewDateHeap(20, "20")
	d4 := timeheap.NewDateHeap(10, "10")
	h := &timeheap.IntHeap{d2, d4}

	heap.Init(h)
	heap.Push(h, d3)
	heap.Push(h, d1)
	// fmt.Printf("%#v \n", (*h)[0])

	for h.Len() > 0 {
		t := (*h)[0].Date.Sub(time.Now())
		fmt.Print((*h)[0], t)
		time.Sleep(t)
		fmt.Printf("%v \n", heap.Pop(h))
	}

}
