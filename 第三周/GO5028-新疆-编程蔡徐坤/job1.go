package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	book := Book{}
	book.Tag = []string{"ccc", "xxx", "kkk"}

	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[0])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[1])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[2])))))
}

// 还有内存地址