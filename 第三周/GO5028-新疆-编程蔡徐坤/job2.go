// 暴力破解
func containsDuplicate(nums []int) bool {
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				return true
			}
		}
	}
	return false
}

// 排序
func containsDuplicate(nums []int) bool {
	sort.Ints(nums)
	for i := 1; i < len(nums); i++ {
		if nums[i] == nums[i-1] {
			return true
		}
	}
	return false
}

// hash map
func containsDuplicate(nums []int) bool {
	temp := make(map[int]int)

	for _, v := range nums {
		if _, key := temp[v]; key {
			return true
		} else {
			temp[v] = 1
		}
	}
	return false
}

// 这是把力扣的题提交了嘛