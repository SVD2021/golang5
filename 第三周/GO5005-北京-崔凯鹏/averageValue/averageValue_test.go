package averageValue

import (
	"fmt"
	"testing"
)

func averageValue(args ...[]int) (averages []float64) {
	for _, student := range args {
		c := 0
		sum := 0
		for _, v := range student {
			c++
			sum += v
		}
		averages = append(averages, float64(sum)/float64(c))
	}
	return
}

func TestAverage(t *testing.T) {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	fmt.Println(averageValue(scores...))
}
