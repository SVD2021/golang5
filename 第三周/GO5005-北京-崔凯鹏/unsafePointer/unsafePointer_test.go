package unsafePointer

import (
	"fmt"
	"testing"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func TestUnsafePointer(t *testing.T) {
	b := Book{Title: "www", Tag: []string{"abc", "def", "hjk"}}
	for i := 0; i <= len(b.Tag)-1; i++ {
		tag := (*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&b.Tag[i]))))
		// fmt.Print(unsafe.Pointer(&b.Tag[i]) )
		fmt.Println(*tag, tag)
	}
}

// 不用写测试，直接完成作业即可
