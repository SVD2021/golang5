package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title string
	Author     string
	Page uint
	Tags     []string
}

func main() {
	book := Book{}
	book.Tags = []string {"aaa", "bbb", "ccc"}

	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tags[0])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tags[1])))))
	fmt.Println(*(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tags[2])))))
}

// 还有内存地址