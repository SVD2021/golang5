package main

import "fmt"

func main() {
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	//总成绩
	sum := 0.0
	//总人数
	count := 0
	for _, v1 := range scores {

		for _, v2 := range v1 {
			sum += float64(v2)
			count++
		}
	}
	//平均分
	fmt.Println(fmt.Sprintf("%.2f", sum/float64(count)))
}

// 仅计算出来一个
