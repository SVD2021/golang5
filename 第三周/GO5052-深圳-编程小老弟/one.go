ackage main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

//根据结构体的内存地址, 计算出Tag的内存地址, 并访问

func main() {

	b := &Book{Tag: []string{"abc", "def", "hjk"}}
	//结构体b的地址
	baddr := unsafe.Pointer(b)
	//tag相对于b结构体的偏移量
	tag := unsafe.Offsetof(b.Tag)
	//计算出Tag的内存地址, 并访问
	fmt.Println(*(*[]string)(unsafe.Pointer(uintptr(baddr) + tag)))
}


// 可以先检查下语法。输出的时候可以考虑格式化输出