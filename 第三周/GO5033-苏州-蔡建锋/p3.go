package main

import "fmt"

/**
 * @Classname p2
 * @Description TODO
 * @author cjf
 * @Date 2021/6/24 9:27
 * @Version V1.0
 */

/*
使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法
*/

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Score    []float64
}

type Class struct {
	Name     string
	Number   int
	Students []Student
}

func avg(c Class) {

	s := make(map[string][]float64)
	var subject = map[string]string{
		"yw": "语文",
		"sx": "数学",
		"yy": "英语",
	}

	for _, v := range c.Students {
		s["yw"] = append(s["yw"], v.Score[0])
		s["sx"] = append(s["sx"], v.Score[1])
		s["yy"] = append(s["yy"], v.Score[2])
	}

	fmt.Print(c.Name + "各科成绩平均分\n")
	tot := 0.0
	avgscore := 0
	for i, v := range s {
		for _, v1 := range v {
			tot += v1
			avgscore = int(tot / 2)

		}
		fmt.Println(subject[i], "\t", avgscore)
	}

}

func main() {
	class1 := Class{
		Name:   "2班",
		Number: 1,
		Students: []Student{
			Student{
				Name:     "ccc",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []float64{90.0, 28.0, 30.0},
			},
			Student{
				Name:     "xxz",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []float64{83.0, 73.0, 63.0},
			},
		},
	}
	class2 := Class{
		Name:   "3班",
		Number: 1,
		Students: []Student{
			Student{
				Name:     "ccc",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []float64{90.0, 28.0, 30.0},
			},
			Student{
				Name:     "xxz",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []float64{23.0, 33.0, 43.0},
			},
		},
	}

	avg(class1)

	avg(class2)

}

// 可以留意下结果
