package main

import "fmt"

/**
 * @Classname p2
 * @Description TODO
 * @author cjf
 * @Date 2021/6/22 15:58
 * @Version V1.0
 */

/*
//  数学   语文   英语
//   88     88    90
//   66
//   ...
//   avg
scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
}
使用二维切片表示一组学生的各科成绩，计算所有学生的平均分
*/

type Studentx struct {
	Name     string   // 名称
	Number   uint16   // 学号
	Subjects []string // 数学  语文  英语
	Score    []int    //  88   99   77
}

type Classx struct {
	Name     string     // 班级名称
	Number   uint8      // 班级编号
	Students []Studentx // 班级学员
}

func main() {
	class1 := Classx{
		Name:   "x班",
		Number: 1,
		Students: []Studentx{
			Studentx{
				Name:     "x",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []int{80, 78, 60},
			},
			Studentx{
				Name:     "y",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Score:    []int{82, 72, 64},
			},
		},
	}

	s := make(map[string][]int)

	for _, v := range class1.Students {
		s["yw"] = append(s["yw"], v.Score[0])
		s["sx"] = append(s["sx"], v.Score[1])
		s["yy"] = append(s["yy"], v.Score[2])
	}

	sum := 0
	for _, v := range s {
		for _, v1 := range v {
			sum += v1

		}
	}
	fmt.Println(sum / 3)

}

// 计算下平均分即可