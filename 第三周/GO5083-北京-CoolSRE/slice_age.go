package main

import (
	"fmt"
)

func Age(numbers []int) (ageNum int) {
	sum := 0
	for _, n := range numbers {
		sum += n
	}
	ageNum = sum / len(numbers)

	return ageNum
}

// 使用二维切片表示一组学生的各科成绩，计算这组学生的学科平均分
func main() {
	scopes := [][]int{
		{88, 88, 90},
		{69, 99, 94},
		{75, 84, 98},
		{93, 84, 66},
	}

	ChineseScope := make([]int, len(scopes))
	MathScope := make([]int, len(scopes))
	ForeignLanguageScope := make([]int, len(scopes))

	for index, scope := range scopes {
		ChineseScope[index] = scope[0]
		MathScope[index] = scope[1]
		ForeignLanguageScope[index] = scope[2]
	}

	ChineseAgeScope := Age(ChineseScope)
	MathAgeScope := Age(MathScope)
	ForeignLanguageAgeScope := Age(ForeignLanguageScope)
	fmt.Printf("ChineseAgeScope: %d, MathAgeScope: %d, ForeignLanguageAgeScope: %d\n", ChineseAgeScope, MathAgeScope, ForeignLanguageAgeScope)
}
