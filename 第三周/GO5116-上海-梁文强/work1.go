package main

import "fmt"

type Book struct {
	Title  string
	Author string
	Page   uint
	Tags   []string
}

func main() {
	a := &Book{
		Title:  "head first go",
		Author: "juju",
		Page:   306,
		Tags:   []string{"learn", "practice", "work"},
	}
	fmt.Println(a.Tags)
}
