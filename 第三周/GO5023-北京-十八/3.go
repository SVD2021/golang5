package main
//使用结构体表示班级和学生，请计算每个班级学科平均分
// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
// Class   名称(Name) 编号(Number) 学员(Students)
// Class   实现一个平均值的方法
import "fmt"

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []float64
}

type Class struct {
	Name     string
	Number   int
	Students []Student
}

func avg() {
	class1 := Class{
		Name:   "1班",
		Number: 1,
		Students: []Student{
			Student{
				Name:     "wtt",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{80.0, 78.0, 60.0},
			},
			Student{
				Name:     "web",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{82.0, 72.0, 64.0},
			},
		},
	}
//使用map集合
	s := make(map[string][]float64)

	for _, v := range class1.Students {
		s["语文"] = append(s["语文"], v.Scores[0])
		s["数学"] = append(s["数学"], v.Scores[1])
		s["英语"] = append(s["英语"], v.Scores[2])
	}

	sum := 0.0
	for _, v := range s {
		for _, v1 := range v {
			sum += v1

		}
	}
	fmt.Println("班级学科平均分是：",sum / 3)
}

func main() {
	avg()
}