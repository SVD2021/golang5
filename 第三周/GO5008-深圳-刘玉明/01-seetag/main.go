package main

import (
	"fmt"
	"unsafe"
	// "reflect"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	var book *Book = &Book{
		Title:  "go-gegin",
		Author: "JadeJones",
		Page:   99,
		Tag:    []string{"go", "book", "Jade"},
	}
	tags := []string{}
	for i := 0; i <= len(book.Tag)-1; i++ {
		a := *(*string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book.Tag[i]))))
		fmt.Println(a, &a)
		tags = append(tags, a)
	}
	fmt.Println(tags)
}

// 不错，继续保持