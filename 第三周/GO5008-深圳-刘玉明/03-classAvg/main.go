package main

import (
	"fmt"
)

type Class struct {
	Name     string
	Number   int
	Students []Student
}

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []float64
}

func Classavg(c Class) {
	studentavg := []float64{}
	var subjectAvg float64
	for i := 0; i <= len(c.Students[0].Subjects)-1; i++ {
		subjectAvg = 0
		for j := 0; j <= len(c.Students)-1; j++ {
			subjectAvg = subjectAvg + float64(c.Students[j].Scores[i])
			// fmt.Println(c.Students[j].Scores[i])
		}
		studentavg = append(studentavg, subjectAvg/float64(len(c.Students)))
		fmt.Printf("班级%s学课%s平均分：%.2f\n", c.Name, c.Students[0].Subjects[i], studentavg[i])
	}

}

func main() {
	var classA = &Class{
		Name:   "go1班",
		Number: 1,
		Students: []Student{
			Student{
				Name:     "xxy",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{80.0, 78.0, 60.0},
			},
			Student{
				Name:     "xxz",
				Number:   2,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{82.0, 72.0, 64.0},
			},
		},
	}
	var classB = &Class{
		Name:   "go2班",
		Number: 2,
		Students: []Student{
			Student{
				Name:     "xyy",
				Number:   1,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{80.0, 78.0, 60.0},
			},
			Student{
				Name:     "xyz",
				Number:   2,
				Subjects: []string{"语文", "数学", "英语"},
				Scores:   []float64{85.0, 72.0, 64.0},
			},
		},
	}
	Classavg(*classA)
	Classavg(*classB)
}

//使用的不错，也可以尝试下不嵌套定义
