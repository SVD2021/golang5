package main

import "fmt"

func main() {

	//  数学   语文   英语
	//   88     88    90
	//   66
	//   ...
	//   avg
	var scores = [5][3]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
		{0, 0, 0},
	}
	for i := 0; i <= 2; i++ { //i 为列
		for j := 0; j <= 3; j++ { //j 为行
			scores[4][i] += scores[j][i] //最后一行存平均数据
		}
		scores[4][i] = scores[4][i] / 4
	}
	fmt.Println("数学语文英语")
	fmt.Println(scores[4])
}


// 可以考虑使用下精度