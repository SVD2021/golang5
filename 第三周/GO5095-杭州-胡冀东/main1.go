package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

func main() {
	var book1 Book // 根据结果推测，这个结构体看着像申明实则为定义，空间分配在堆内， 为堆变量
	fmt.Println(unsafe.Pointer(&book1))
	book1 = Book{
		Title:  "Bible",
		Author: "God",
		Page:   1,
		Tag: []string{
			"In the beginning God created the heaven and the earth.",
			"And the Spirit of God moved upon the face of the waters.",
			"And God said, Let there be light: and there was light.",
			"And God saw the light, that it was good: and God divided the light from the darkness.",
			"And God called the light Day, and the darkness he called Night. And the evening and the morning were the first day.",
		},
	}
	fmt.Println(unsafe.Pointer(&book1.Tag))
	fmt.Println(unsafe.Pointer(uintptr(unsafe.Pointer(&book1)) + unsafe.Sizeof(book1.Title) + unsafe.Sizeof(book1.Author) + unsafe.Sizeof(book1.Page)))
	fmt.Println((*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(&book1)) + unsafe.Sizeof(book1.Title) + unsafe.Sizeof(book1.Author) + unsafe.Sizeof(book1.Page))))

}

// 可以格式化下输出