package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Class struct {
	Name     string
	Number   int
	Students map[int]*Student
}

type Student struct {
	Name     string
	Number   int
	Subjects []string
	Scores   []float64
}

func NewStudent(Name string, Number int, Scores []float64) *Student {
	return &Student{
		Name:     Name,
		Number:   Number,
		Subjects: []string{"语文", "数学", "英语"},
		Scores:   Scores,
	}
}

func NewClass(Name string, Number int, Students map[int]*Student) Class {
	return Class{
		Name:     Name,
		Number:   Number,
		Students: Students,
	}
}

// 语文,数学,英语平均分
func (c Class) CalcAvg() (string, string, string) {
	var totalChina float64
	var totalMath float64
	var totalEng float64
	for _, v := range c.Students {
		totalChina += v.Scores[0]
		totalMath += v.Scores[1]
		totalEng += v.Scores[2]
	}
	count := float64(len(c.Students))
	return fmt.Sprintf("%.2f", totalChina/count), fmt.Sprintf("%.2f", totalMath/count), fmt.Sprintf("%.2f", totalEng/count)
}

func GetScore() float64 {
	return rand.Float64() + float64(rand.Intn(99))
}

func main() {
	timeNow := time.Now().Unix()
	rand.Seed(timeNow)

	// 生成100个随机数,代表有100个学生
	var c1 map[int]*Student = make(map[int]*Student, 100)
	for i := 1; i <= 100; i++ {
		c1[i] = NewStudent(fmt.Sprintf("阿强%d", i), i, []float64{GetScore(), GetScore(), GetScore()})
	}
	class1 := NewClass("一班", 1, c1)
	fmt.Printf("%#v", class1)
	// 打印平均分
	fmt.Println(class1.CalcAvg())
}

// 可以使用格式化输出