package main

import (
	"fmt"
	"testing"
)





//使用结构体表示班级和学生，请计算每个班级学科平均分
//Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
//Class   名称(Name) 编号(Number) 学员(Students)
//Class   实现一个平均值的方法


type Class struct {
	ClassName   string // 班级名称
	ClassNumber int // 班级编号
}

type Student struct {
	myClass    Class     // 嵌套班级
	UserName   string    // 学生姓名
	UserNumber int       // 学生编号
	Subjects   []string  //学生科目
	Scores     []float64 // 学生成绩
}

func (c Class) avg(s Student) float64 {
	var count float64
	for i,v := range s.Subjects{
		fmt.Printf("%v 分数为：%v\n",v ,s.Scores[i])
		count += s.Scores[i]
	}
	return count / float64(len(s.Scores))
}

func TestAvg1(t *testing.T) {
	s := Student{
		myClass: Class{
			ClassName: "一班",
			ClassNumber: 1,
		},
		UserName: "zhaoqi",
		UserNumber: 1,
		Subjects: []string {
			"数学",
			"英语",
			"语文",
		},
		Scores: []float64 {
			88,82,91,
		},
	}

	fmt.Println(s.myClass.avg(s))

}

// 不用写单元测试，直接完成作业即可