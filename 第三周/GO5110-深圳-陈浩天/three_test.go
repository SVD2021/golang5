package three

import (
    "testing"
)

var (
	scores = [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}

	count = 0
	sub   = 0
)


// 求所有学生平均分
func TestAvg(t *testing.T) {
	for _, v := range scores {
		for _, k := range v {
			count += k
			sub++
		}
	}
	fmt.Printf("所有学生平均分：%v\n", count/sub)
}
