package main

import (
	"fmt"
	"unsafe"
)

type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
}

// b := &Book{Tag: []string{"abc", "def", "hjk"}}
// 根据结构体的内存地址, 计算出Tag的内存地址, 并访问

func main() {
	b := &Book{Tag: []string{"abc", "def", "hjk"}}
	bStringSize := unsafe.Sizeof(b.Title) * 2
	bPageSize := unsafe.Sizeof(b.Page)
	p := (*[]string)(unsafe.Pointer(uintptr(unsafe.Pointer(b)) + bStringSize + bPageSize))
	fmt.Println(*p)
}