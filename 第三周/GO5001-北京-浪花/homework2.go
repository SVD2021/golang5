package main

import "fmt"

//使用二维切片表示一组学生的各科成绩，计算这组学生的学科平均分

var scores = [][]int{
{88, 88, 90},
{66, 99, 94},
{75, 84, 98},
{93, 77, 66},
}

func main() {
	var MathSum, ChineSum, EnglishSum int
	fmt.Printf("%s%4s%4s\n","语文","数学","英语")
	for i := 0; i < 4; i++ {
		for j := 0; j < 3; j++ {
			fmt.Printf("%-6d", scores[i][j])
			if j == 0 {
				MathSum += scores[i][j]
			}
			if j == 1 {
				ChineSum += scores[i][j]
			}
			if j == 2 {
				EnglishSum += scores[i][j]
			}
		}
		fmt.Println()
	}
	fmt.Println("------avg------")

	fmt.Printf("%-6d%-6d%-6d\n", MathSum/4, ChineSum/4, EnglishSum/4)

}