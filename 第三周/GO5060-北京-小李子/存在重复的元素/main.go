package main

import (
	"fmt"
)

/*
存在重复的元素
  给定一个整数数组，判断是否存在重复的元素
  如果存在一值在数组中出现至少两次，函数返回true。如果数组中没个元素都不相同 则返回false
*/

func pd(array []int) bool {
	Mreturn := map[int]int{}
	for _, i := range array {
		if _, ok := Mreturn[i]; !ok {
			Mreturn[i] = i
		} else {
			fmt.Printf("数值: %d 已多次存在！\n", Mreturn[i])
			return true
		}
	}
	return false
}

func main() {
	array := []int{1, 2, 2, 5}
	status := pd(array)
	fmt.Printf("返回状态: %v\n", status)
}
