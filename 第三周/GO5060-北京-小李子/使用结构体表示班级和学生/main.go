package main

import (
	"fmt"
)

/*
使用结构体表示班级和学生，请计算每个班级学科平均分
	Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
	Class   名称(Name) 编号(Number) 学员(Students)
	Class   实现一个平均值的方法
*/

type Student struct {
	Name     string
	Number   int64
	Subjects [3]string
	Scores   [3]int
}

type Class struct {
	Name     string
	Number   int64
	Students []Student
}

func (c *Class) Avg(lst []int) float64 {
	var avg int
	for _, v := range lst {
		avg += v
	}
	return float64(avg) / float64(len(lst))
}

func main() {
	c := Class{
		Name:   "十二班",
		Number: 12,
		Students: []Student{
			Student{
				Name:     "张三",
				Number:   1,
				Subjects: [3]string{"数学", "语文", "英语"},
				Scores:   [3]int{97, 80, 65},
			},
			Student{
				Name:     "里斯",
				Number:   1,
				Subjects: [3]string{"数学", "语文", "英语"},
				Scores:   [3]int{30, 60, 80},
			},
		},
	}
	var m = map[string][]int{}
	var nameDict = map[string]string{
		"sx": "数学",
		"yw": "语文",
		"yy": "英语",
	}
	fmt.Printf("班级: %s\t 班级编码: %v\n", c.Name, c.Number)
	fmt.Print("\n", "学生成绩如下:\n")
	fmt.Printf("%5s\t%5s\t%5s\n", "数学", "语文", "英语")
	for _, v := range c.Students {
		fmt.Printf("%5d\t%5d\t%5d\n", v.Scores[0], v.Scores[1], v.Scores[2])
		m["sx"] = append(m["sx"], v.Scores[0])
		m["yw"] = append(m["yw"], v.Scores[1])
		m["yy"] = append(m["yy"], v.Scores[2])
	}
	fmt.Print("\n", "平均分:\n")
	for i, v := range m {
		value := c.Avg(v)
		fmt.Println(nameDict[i], ":", "\t", value)
	}

}
// 居然想到了精度，不错