package main

import (
	"fmt"
)

/*
使用二维切片表示一组学生的各科成绩，计算所有学生的平均分
//  数学   语文   英语
//   88     88    90
//   66
//   ...
//   avg
scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
}
*/

func avg(lst []int) (string, int) {
	var avg int
	for _, v := range lst {
		avg += v
	}
	return fmt.Sprintf("%0.2f", float32(avg)/float32(len(lst))), avg
}

func main() {
	var m = map[string][]int{}
	var nameDict = map[string]string{
		"sx": "数学",
		"yw": "语文",
		"yy": "英语",
	}
	scores := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
	}
	fmt.Println("学生成绩:")
	fmt.Printf("%5s\t%5s\t%5s\n", "数学", "语文", "英语")
	for _, v := range scores {
		fmt.Printf("%5d\t%5d\t%5d\n", v[0], v[1], v[2])
		m["sx"] = append(m["sx"], v[0])
		m["yw"] = append(m["yw"], v[1])
		m["yy"] = append(m["yy"], v[2])
	}
	fmt.Print("\n", "平均分:\n")
	sum := 0
	for i, v := range m {
		value, s := avg(v)
		sum += s
		fmt.Println(nameDict[i], ":", "\t", value)
	}
	fmt.Print("\n", "总平均分\n")
	fmt.Printf("%0.2f", float32(sum)/float32(len(scores)))
}

// 居然想到了使用格式化输出。不过换行也要考虑全