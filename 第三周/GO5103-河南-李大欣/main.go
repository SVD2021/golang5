package main

import "fmt"

/*
	1.通过内存地址访问Tag的值
	type Book struct {
	Title  string
	Author string
	Page   uint
	Tag    []string
	}

	// b := &Book{Tag: []string{"abc", "def", "hjk"}}
	// 根据结构体的内存地址, 计算出Tag的内存地址, 并访问
	思路: 指针 与 offset
*/

/*
	2.使用二维切片表示一组学生的各科成绩，计算所有学生的平均分
	//  数学   语文   英语
	//   88     88    90
	//   66
	//   ...
	//   avg
	scores = [][]int{
	{88, 88, 90},
	{66, 99, 94},
	{75, 84, 98},
	{93, 77, 66},
	}
*/
func studentSubjectAvg(){
	score := [][]int{
		{88, 88, 90},
		{66, 99, 94},
		{75, 84, 98},
		{93, 77, 66},
		{90, 70, 60},
	}
	fmt.Printf("%s\t%s\t%s\n","数学","语文","英文")
	var math,chinese,english int
	for _, student := range score {
		math+= student[0]
		chinese+= student[1]
		english+= student[2]
		fmt.Printf("%d\t%d\t%d\n",student[0],student[1],student[2])
	}
	fmt.Println("平均分是：")
	fmt.Printf("%d\t%d\t%d\n", math/len(score),chinese/len(score),english/len(score))
}

/*
	3.使用结构体表示班级和学生，请计算每个班级学科平均分
	// Student 名称(Name) 学号(Number) 科目(Subjects) 成绩(Scores)
	// Class   名称(Name) 编号(Number) 学员(Students)
	// Class   实现一个平均值的方法
 */

type Student struct {
	Name string
	Number int
	Scores []struct{
		Subject string
		Score int
	}
}

type Class struct {
	Name string
	Number int
	Students []*Student
}

func newClass (name string, number int) *Class {
	return &Class{
		Name: name,
		Number: number,
	}
}

func newStudent (name string, number int) *Student {
	return &Student{
		Name: name,
		Number: number,
	}
}

func (s *Student) addStudentSubjectAndScore(subject string, score int) {
	info := struct {
		Subject string
		Score int
	}{
		Subject: subject,
		Score: score,
	}
	s.Scores = append(s.Scores, info)
}

func (c *Class) addStudentToClass(s *Student) {
	c.Students = append(c.Students, s)
}

func (c *Class) calcSubjectAvg(subjectName string) {
	var chinese, math,english int
	switch subjectName {
	case "语文":
		for _, student := range c.Students {
			for _, Subjects := range student.Scores {
				if Subjects.Subject == "语文" {
					chinese += Subjects.Score
				}
			}
		}
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, subjectName, chinese/len(c.Students))
	case "数学":
		for _, student := range c.Students {
			for _, Subjects := range student.Scores {
				if Subjects.Subject == "数学" {
					math += Subjects.Score
				}
			}
		}
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, subjectName, math/len(c.Students))
	case "英语":
		for _, student := range c.Students {
			for _, Subjects := range student.Scores {
				if Subjects.Subject == "英语" {
					english += Subjects.Score
				}
			}
		}
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, subjectName, english/len(c.Students))
	case "所有":
		for _, student := range c.Students {
			for _, Subjects := range student.Scores {
				if Subjects.Subject == "英语" {
					english += Subjects.Score
				}
				if Subjects.Subject == "数学" {
					math += Subjects.Score
				}
				if Subjects.Subject == "语文" {
					chinese += Subjects.Score
				}
			}
		}
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, "语文", chinese/len(c.Students))
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, "英语", english/len(c.Students))
		fmt.Printf("班级编号: %d, 班级名称：%s, 学科: %s, 平均分是：%d\n",c.Number, c.Name, "数学", math/len(c.Students))
	default:
		fmt.Println("学科不存在！")
	}
}





func main(){
	//fmt.Println("Question 1:")
	//studentSubjectAvg()
	//fmt.Println()

	// fmt.Println("Question 2："）
	// 创建班级
	c1 := newClass("Golang 5", 1005)

	// 创建学生并录入分数
	s1 := newStudent("daxin",1001)
	s1.addStudentSubjectAndScore("语文",99)
	s1.addStudentSubjectAndScore("数学", 100)
	s1.addStudentSubjectAndScore("英语", 95)

	// 学生关联班级
	c1.addStudentToClass(s1)

	// 计算学科平均值
	c1.calcSubjectAvg("所有")
}