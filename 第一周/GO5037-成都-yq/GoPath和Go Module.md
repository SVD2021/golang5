# GOPath
传统的代码组织架构, 在$GOPATH目录下有3个文件夹：
- bin: 放置build后的二进制文件
- pkg：存放编译后的静态库文件
- src：存放源代码文件
在GOPath模式下，引用其他包，需import src目录下面的相对路径，不会自动下载依赖包
# GO Module
Go Module模式需使用go init初始化项目，产生go.mod文件，go.mod
文件里面记录项目依赖的包和版本，如果本地没有的包，会自动去网上下载,下载到$GOPATH/pkg/mod目录里面