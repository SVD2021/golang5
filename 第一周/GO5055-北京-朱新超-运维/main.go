package main //入口包

import "fmt" //引用GOROOT下的fmt
//import "pkg/"

/* var (        //全局变量
	i int = 9   //定义变量时指定变量类型
	j = "asdf"  //根据定义推导变量类型
)
多行注释，多变量定义
*/

func main() { //入库函数

	fmt.Println("Hello World!")
	//fmt.Println(i , j)  单行注释

	var a int = 10
	fmt.Println(a)
	a = 20  //变量赋值
	b := 30 //定义局部变量并赋值
	c := "asdf"
	fmt.Println(a, b, c)

}

// 可以尝试下不同的打印方法，另外只提交作业代码即可