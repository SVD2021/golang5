什么是GoPath：
	Golang的工作空间，所有的Go文件，都需要放在GoPath下的src目录下才能够编译运行。

什么是GoModule：
	GoModule就是一个用来取代GoPath的Golang的工作空间。
	是Golang在1.11版本初步引入的概念，在1.12版本中正是开始使用，所以如果需要使用GoModule，那么需要保证你的Golang的版本在1.12或以上。，Golang1.11和1.12版本虽然已经引入了GoModule的概念，但是GoModule是默认不开启的，如果需要开启，那么需要配置一个环境变量：go env -w  GO111MODULE=on，默认是off。而在Golang1.13及以上的版本中，GoModule的默认配置为auto(mac 默认是空)，即GoModule会通过你的目录下是否有go.mod文件来判断是否开启GoModule。所以Golang1.13+的版本中我们就不需要配置GO111MODULE属性了。
为什么要用GoModule:
	1:第三方依赖的包和我们自己的Golang包混在一起，会给我们的项目文件管理带来一定的麻烦。
	2:不同的GoPath都需要下载依赖，那么磁盘中重复的依赖就会非常多，会占用我们大量的磁盘空间。

有了GoModule之后，GoPath我们用来存放我们从网上拉取的第三方依赖包;GoModule我们用来存放我们自己的Golang项目文件，当我们自己的项目需要依赖第三方的包的时候，我们通过GoModule目录下的一个go.mod文件来引用GoPath目录src包下的第三方依赖即可。

主要目的：
	在引入GoModule之后，我们不用直接在GoPath目录进行编程，而是把GoPath作为一个第三方依赖包的仓库，我们真正的工作空间在GoModule目录下。