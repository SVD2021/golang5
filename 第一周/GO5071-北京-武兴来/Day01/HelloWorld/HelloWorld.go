/*
// 单行注释
多行注释
*/
// Create time: 2021-06-03
// FileName: HelloWorld.go
/*
Run: go run HelloWorld.go
Build: go  build -o HelloWorld.exe  HelloWorld.go
    -o: 指定编译后的文件名
*/
package main

import "fmt"

func main() {
	fmt.Println("Hello World")
}
// 可以尝试下不同的打印方法