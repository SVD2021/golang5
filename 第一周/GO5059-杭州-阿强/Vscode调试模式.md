1. 运行调试模式



![image-20210604144431063](./image/vscode-debug-0.png)

2. 编辑需要调试的代码

![image-20210604144349906](./image/vscode-debug-1.png)

3. ![](./image/vscode-debug-2.png)

4. 给代码打断点，并且查看变量的值

![](./image/vscode-debug-3.png)

