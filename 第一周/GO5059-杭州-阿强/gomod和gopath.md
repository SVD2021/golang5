## 1. gopath

GOPATH 目录规定了三个子目录

* **src** 存放源代码
* **pkg** 编译后生成的静态库；多个静态库通过连接器连接成我们想要的库
* **bin** 编译后生成的可执行文件





使用gopath遇到的问题：

1. 代码开发必须在gopath下面的src目录进行
2. 依赖包下载手动
3. 依赖包没有版本管理







## 2. go mod

go modules是golang1.11新加的内容



### 2.1 如何使用go mod

首先需要设置go mod 功能开启和设置 go porxy 变量

```
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct // 使用七牛云代理
```

重新确认下配置

```
go env 
```



### 2.2 GO111MODULE 

GO111MODULE 有是三个值：off，on，auto

* off：关闭go module 功能，使用go path功能，主要是为了兼容使用gopath的老项目
* on：使用go module功能
* auto：根据当前目录来决定是否启用go module



### 2.3 使用go mod管理一个项目

#### 2.3.1 初始化一个项目

```
go mod init 项目名称
```

