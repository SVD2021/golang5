# Golang中GoPath和GoModule的区别



[toc]

go语言有2种工程结构:

- GOPATH工程结构
- GO MODULE工程结构

### 一、GoPath

GoPath是Golang的工作空间，所有的Go文件，都需要放在GoPath下的src目录下才能够编译运行

而GoPath的工作目录有指定的文件夹构成

```
$ ls
bin/  pkg/  src/
```

- src 存放源代码（比如：.go .c .h .s等）
- pkg 编译后生成的静态库（比如：.a）, 多个静态库文件通过连接器连接 最终构成我们要得目标文件
- bin 编译后生成的可执行文件（为了方便，可以把此目录加入到 PATH变量中，如果有多个gopath，那么使用PATH变量中，如果有多个gopath，那么使用{GOPATH//://bin:}/bin添加所有的bin目录）

在GoPath工程结构里面，在项目中使用第三方类库的时候，可以使用`go get`命令从网下直接拉去第三方类库的包，而拉取下来的包就会直接下载到我们的GoPath目录下的src包下。

这样就导致了一个问题，我们自己的Golang代码，和第三方的Golang文件混在了一起，这对于我们管理Golang项目的包显然是非常麻烦的，而且每个如果项目都需要同样的依赖，那么我们就会在不同的GoPath的src中下载大量重复的第三方依赖包，这同样会占用大量的磁盘空间。

**我们给不同的项目设置不同的GoPath，优点非常明显：**

便于管理项目，每个项目都是不同的GoPath，这对于我们管理多个Golang项目而言，能够非常清晰的处理项目结构。如果我们把所有项目都放在同一个GoPath的src包下，那么项目的结构就会变得非常混乱，难以管理。



**但是当我们需要依赖第三方的包的时候，不同的项目设置不同的GoPath的缺点也非常明显：**

第三方依赖的包和我们自己的Golang包混在一起，会给我们的项目文件管理带来一定的麻烦。
不同的GoPath都需要下载依赖，那么磁盘中重复的依赖就会非常多，会占用我们大量的磁盘空间。
所以，究竟是设置一个GoPath目录，解决依赖重复的问题，还是设置不同的GoPath目录，解决Golang项目结构混乱的问题，这是一个有争议性的问题。

### 二、GoModule

GoModule是Golang在1.11版本初步引入的概念，在1.12版本中正是开始使用，所以如果需要使用GoModule，那么需要保证你的Golang的版本在1.12或以上。
另外需要说一下，Golang1.11和1.12版本虽然已经引入了GoModule的概念，但是GoModule是默认不开启的，如果需要开启，那么需要配置一个环境变量：`GO111MODULE=on`，默认是`off`。

而在Golang1.13及以上的版本中，GoModule的默认配置为auto，即GoModule会通过你的目录下是否有go.mod文件来判断是否开启GoModule。所以Golang1.13+的版本中我们就不需要配置GO111MODULE属性了。

#### 2.1 那么究竟什么是GoModule?

- 其实说得直白一下，GoModule就是一个用来取代GoPath的Golang的工作空间。
- Go Modules解决方式很像是Java看到Maven的做法，将第三方程式库储存在本地的空间，并且给程式去引用。

#### 2.2 设定GO111MODULE环境变数

```
$ go env GO111MODULE
auto
$ go env GO111MODULE=on
$ go env GO111MODULE=off
$ go env GO111MODULE=auto
```

- auto 默认值，go命令会根据当前目录来决定是否启用modules功能。需要满足两种情形： 该专案目录不在`GOPATH/src/`下 当前或上一层目录存在go.mod档案
- on go命令会使用modules，而不会GOPATH目录下查找。
- off go命令将不会支持module功能，寻找套件如以前GOPATH的做法去寻找。

#### 2.3 初始化mod

```
go mod init <module name>
```

`<module name>`可填可不填，不填的话预设就是采用专案资料夹的名称。

当初始化命令执行完毕之后，会在go_module目录下生成一个go.mod文件，该文件就是用来引入GoPath目录下的第三方依赖的文件。

当我们需要引入GoPath目录下的第三方依赖包的时候，只需要在go.mod目录下添加依赖名称，GoModule就会自动帮我们把第三方依赖包下载到GoPath目录下。

### 总结

GoPath所引出的问题，就是因为第三方类库的包所导致的，所以我们在有了GoModule之后，GoPath和GoModule就分别负责不同的职责，共同为我们的Golang项目服务。

GoPath我们用来存放我们从网上拉取的第三方依赖包。 GoModule我们用来存放我们自己的Golang项目文件，当我们自己的项目需要依赖第三方的包的时候，我们通过GoModule目录下的一个go.mod文件来引用GoPath目录pkg包下的mod文件夹下的第三方依赖即可。

这样依赖，既解决了原来只能局限在GoPath目录src包下进行编程的问题，也解决了第三方依赖包难以管理和重复依赖占用磁盘空间的问题。



