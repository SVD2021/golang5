package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"
)

// 字符串拼接benchmark
// 对比以下几个方法
// func xxx(n int ,str string) string{
// }
// 使用+拼接
// 使用fmt.sprinf拼接
// 使用strings.builder拼接
// 使用bytes.buffer拼接
// 使用[]byte拼接

// 字符库，共52个，大小写各26个
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

//生成基础字符的方法
func str_base(n int) {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
}

//使用+拼接：
func str_plussign(n int) string {
	rand.Seed(time.Now().UnixNano())
	str := ""
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		str = str + string(b[i])
	}
	return str
}

//使用fmt.sprinf拼接
func str_sprinf(n int) string {
	rand.Seed(time.Now().UnixNano())
	str := ""
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		str = fmt.Sprintf("%s%s", str, string(b[i]))
	}
	return str
}

//使用strings.builder拼接
func str_builder(n int) string {
	rand.Seed(time.Now().UnixNano())
	var str strings.Builder
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		str.WriteString(string(b[i]))
	}
	return str.String()
}

//使用bytes.buffer拼接
func str_buffer(n int) string {
	rand.Seed(time.Now().UnixNano())
	var buffer bytes.Buffer
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		buffer.WriteString(string(b[i]))
	}
	return buffer.String()
}

//使用[]byte拼接
func str_slicebyte(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

//封装测试函数
func benchmarkstr_plussign(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		str_plussign(i)
	}

}

func benchmarkstr_sprinf(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		str_sprinf(i)
	}

}
func benchmarkstr_builder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		str_builder(i)
	}

}
func benchmarkstr_buffer(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		str_buffer(i)
	}

}
func benchmarkstr_slicebyte(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		str_slicebyte(i)
	}

}

// Benchmark测试
func BenchmarkStr_plussign(b *testing.B) {benchmarkstr_plussign(1000,b)}
func BenchmarkStr_sprinf(b *testing.B) {benchmarkstr_sprinf(1000,b)}
func BenchmarkStr_builder(b *testing.B) {benchmarkstr_builder(1000,b)}
func BenchmarkStr_buffer(b *testing.B) {benchmarkstr_buffer(1000,b)}
func BenchmarkStr_slicebyte(b *testing.B) {benchmarkstr_slicebyte(1000,b)}

