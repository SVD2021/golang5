package joiner

//- 使用[]byte拼接
func StrByte(str string) (res string) {
	b := make([]byte, len(str))
	for i := 0; i < len(str); i++ {
		b[i] = str[i]
	}
	return string(b)
}
