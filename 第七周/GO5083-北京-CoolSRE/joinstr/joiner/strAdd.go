package joiner

//- 使用+拼接
func strAdd(str string) (res string) {
	for i := 0; i < len(str); i++ {
		res += string(str[i])
	}
	return res
}
