package joiner

import "fmt"

//- 使用fmt.sprinf拼接
func StrSprinf(str string) (res string) {
	for i := 0; i < len(str); i++ {
		res = fmt.Sprintf("%s%s", res, string(str[i]))
	}
	return res
}
