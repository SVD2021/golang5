package joiner

import "bytes"

//- 使用bytes.buffer拼接
func StrBytesBuffer(str string) (res string) {
	sb := bytes.Buffer{}
	for i := 0; i < len(str); i++ {
		sb.WriteString(string(str[i]))
	}
	return sb.String()
}
