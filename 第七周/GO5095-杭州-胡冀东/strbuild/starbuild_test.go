package strbuild

import (
	"fmt"
	"testing"
)

func TestGenbyte(t *testing.T) {
	fmt.Printf("%c \n", GenByte())
}
func TestStrSprint(t *testing.T) {
	str := StrSfprint(10, GenRandomString(10))
	fmt.Println(str)
}
func TestGenRandomString(t *testing.T) {
	fmt.Println(GenRandomString(1000))
}
func TestStrStringBuilder(t *testing.T) {
	str := StrStringBuilder(10, GenRandomString(10))
	fmt.Println(str)
}
func TestStrBytebuffer(t *testing.T) {
	str := StrBytebuffer(10, GenRandomString(10))
	fmt.Println(str)
}

func TestStrByteArray(t *testing.T) {
	str := StrByteArray(10, GenRandomString(10))
	fmt.Println(str)
}
func BenchmarkGenRandomString(b *testing.B) {
	for i := 0; i <= b.N; i++ {
		GenRandomString(i)
	}
	/*
		staro@star15g MINGW64 /e/golang3/day7/homework1/strbuild
		$ go test -v -bench . -run none -benchmem
		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkGenRandomString
		BenchmarkGenRandomString-16        13353            118578 ns/op           14129 B/op          2 allocs/op
		PASS
		ok      homework1/strbuild      2.832s

	*/
}

//+操作
func BenchmarkStrAdd(b *testing.B) {
	str := GenRandomString(10)
	for i := 0; i < b.N; i++ {
		Stradd(i, str)
	}
	/*
		Running tool: D:\Program Files\Go\bin\go.exe test -benchmem -run=^$ -bench ^(BenchmarkStrAdd)$ homework1/strbuild -v

		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkStrAdd
		BenchmarkStrAdd-16
		   10000	  16317080 ns/op	178592330 B/op	    5005 allocs/op
		PASS
		ok  	homework1/strbuild	163.497s
	*/
}
func BenchmarkStrSprint(b *testing.B) {
	str := GenRandomString(10)
	for i := 0; i < b.N; i++ {
		StrSfprint(i, str)
	}
	/*
		Running tool: D:\Program Files\Go\bin\go.exe test -benchmem -run=^$ -bench ^(BenchmarkStrSprint)$ homework1/strbuild -v

		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkStrSprint
		BenchmarkStrSprint-16
		   10000	  25165813 ns/op	228471837 B/op	   16399 allocs/op
		PASS
		ok  	homework1/strbuild	251.901s
	*/
}
func BenchmarkStringBuilder(b *testing.B) {
	str := GenRandomString(10)
	for i := 0; i < b.N; i++ {
		StrStringBuilder(i, str)
	}
	/*
		Running tool: D:\Program Files\Go\bin\go.exe test -benchmem -run=^$ -bench ^(BenchmarkStringBuilder)$ homework1/strbuild -v

		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkStringBuilder
		BenchmarkStringBuilder-16
		   27876	    116465 ns/op	  687188 B/op	      24 allocs/op
		PASS
		ok  	homework1/strbuild	3.931s
	*/
}

func BenchmarkStrBytebuffer(b *testing.B) {
	str := GenRandomString(10)
	for i := 0; i < b.N; i++ {
		StrBytebuffer(i, str)
	}
	/*
		Running tool: D:\Program Files\Go\bin\go.exe test -benchmem -run=^$ -bench ^(BenchmarkStrBytebuffer)$ homework1/strbuild -v

		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkStrBytebuffer
		BenchmarkStrBytebuffer-16
		   20374	    119468 ns/op	  422078 B/op	      12 allocs/op
		PASS
		ok  	homework1/strbuild	3.338s
	*/
}
func BenchmarkStrByteArray(b *testing.B) {
	str := GenRandomString(10)
	for i := 0; i < b.N; i++ {
		StrByteArray(i, str)
	}
	/*
		Running tool: D:\Program Files\Go\bin\go.exe test -benchmem -run=^$ -bench ^(BenchmarkStrByteArray)$ homework1/strbuild -v

		goos: windows
		goarch: amd64
		pkg: homework1/strbuild
		cpu: Intel(R) Core(TM) i9-10885H CPU @ 2.40GHz
		BenchmarkStrByteArray
		BenchmarkStrByteArray-16
		   31470	    118311 ns/op	  322282 B/op	       2 allocs/op
		PASS
		ok  	homework1/strbuild	4.825s
	*/
}
