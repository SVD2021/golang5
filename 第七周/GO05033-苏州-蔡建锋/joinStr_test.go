package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"testing"
)

/**
 * @Classname joinStr
 * @Description TODO
 * @author cjf
 * @Date 2021/7/19 22:10
 * @Version V1.0
 */

/*
	- 使用+拼接
	- 使用fmt.sprinf拼接
	- 使用strings.builder拼接
	- 使用bytes.buffer拼接
	- 使用[]byte拼接
	- 生成基本字符串的函数如下
*/

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

//使用fmt.sprinf拼接
func randomStrSprinf(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return fmt.Sprintf("#{b}")
}

//使用[]byte拼接字符串
func randomStrByte(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

//使用strings.builder拼接
func randomStrSbuilder(n int) string {
	s := strings.Builder{}
	for i := 0; i < n; i++ {
		s.WriteByte(letterBytes[rand.Intn(len(letterBytes))])
	}
	return s.String()
}

//使用bytes.builder拼接
func randomStrBbuilder(n int) string {
	b := bytes.Buffer{}
	for i := 0; i < n; i++ {
		b.WriteByte(letterBytes[rand.Intn(len(letterBytes))])
	}
	return b.String()
}

//使用+拼接字符串
func randomStringJoin(n int) string {
	var j string
	for i := 0; i < len(letterBytes); i++ {
		j += string(letterBytes[rand.Intn(len(letterBytes))])
	}
	return j
}

func benchmarkrandomStrSprinf(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStrSprinf(i)
	}
}

func benchmarkrandomStrByte(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStrByte(i)
	}
}

func benchmarkrandomStrSbuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStrSbuilder(i)
	}
}

func benchmarkrandomStrBbuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStrBbuilder(i)
	}
}

func benchmarkrandomStringJoin(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		randomStringJoin(i)
	}
}

//测试
func BenchmarkRandomStringSprinf100(b *testing.B)    { benchmarkrandomStrSprinf(100, b) }
func BenchmarkRandomStringSprinf1000(b *testing.B)   { benchmarkrandomStrSprinf(1000, b) }
func BenchmarkRandomStringSprinf10000(b *testing.B)  { benchmarkrandomStrSprinf(10000, b) }
func BenchmarkRandomStringSprinf100000(b *testing.B) { benchmarkrandomStrSprinf(100000, b) }

func BenchmarkRandomRandomStringByte100(b *testing.B)    { benchmarkrandomStrByte(100, b) }
func BenchmarkRandomRandomStringByte1000(b *testing.B)   { benchmarkrandomStrByte(1000, b) }
func BenchmarkRandomRandomStringByte10000(b *testing.B)  { benchmarkrandomStrByte(10000, b) }
func BenchmarkRandomRandomStringByte100000(b *testing.B) { benchmarkrandomStrByte(100000, b) }

func BenchmarkRandomRandomStringSbuilder100(b *testing.B) {
	benchmarkrandomStrSbuilder(100, b)
}
func BenchmarkRandomRandomStringSbuilder1000(b *testing.B) {
	benchmarkrandomStrSbuilder(1000, b)
}
func BenchmarkRandomRandomStringSbuilder10000(b *testing.B) {
	benchmarkrandomStrSbuilder(10000, b)
}
func BenchmarkRandomRandomStringSbuilder100000(b *testing.B) {
	benchmarkrandomStrSbuilder(100000, b)
}
func BenchmarkRandomRandomStringSbuilder1000000(b *testing.B) {
	benchmarkrandomStrSbuilder(100000, b)
}

func BenchmarkRandomRandomStringBbuilder100(b *testing.B) {
	benchmarkrandomStrBbuilder(100, b)
}
func BenchmarkRandomRandomStringBbuilder1000(b *testing.B) {
	benchmarkrandomStrBbuilder(1000, b)
}
func BenchmarkRandomRandomStringBbuilder10000(b *testing.B) {
	benchmarkrandomStrBbuilder(10000, b)
}
func BenchmarkRandomRandomStringBbuilder100000(b *testing.B) {
	benchmarkrandomStrBbuilder(100000, b)
}

func BenchmarkRandomRandomStringJoin10(b *testing.B)    { benchmarkrandomStringJoin(10, b) }
func BenchmarkRandomRandomStringJoin100(b *testing.B)   { benchmarkrandomStringJoin(100, b) }
func BenchmarkRandomRandomStringJoin1000(b *testing.B)  { benchmarkrandomStringJoin(1000, b) }
func BenchmarkRandomRandomStringJoin10000(b *testing.B) { benchmarkrandomStringJoin(10000, b) }
