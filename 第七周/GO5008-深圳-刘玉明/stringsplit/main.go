package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strings"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// func randomString(n int) string {
// 	b := make([]byte, n)
// 	for i := range b {
// 		b[i] = letterBytes[rand.Intn(len(letterBytes))]
// 	}
// 	return string(b)
// }

// strAdd 使用 + 进行拼接
func strAdd(n int) string {
	b := make([]byte, n)
	var addtmp string
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
		addtmp += string(b[i])
	}
	return addtmp
}

// strSprintf 使用 fmt.Sprintf 进行拼接
func strSprintf(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return fmt.Sprintf("%s", b)
}

// strBuilder 使用 strings.builder 拼接
func strBuilder(n int) string {
	b := make([]byte, n)
	var c strings.Builder
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	fmt.Fprintf(&c, "%s", b) // 这种就可以写入很多类型 只需要format
	// c.WriteString(string(b))  另外一种写法
	return c.String()
}

// strBuffer 使用bytes.Buffer 拼接字符串
func strBuffer(n int) string {
	b := make([]byte, n)
	var c bytes.Buffer
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	c.WriteString(string(b))
	return c.String()
}

// strByter 使用Byte 拼接字符串
func strByte(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
