package main

import "testing"

// benchmarkstrAdd 用于测试 strAdd
func benchmarkstrAdd(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		strAdd(i)
	}
}

// benchmarkstrSprintf 用于测试 strSprintf
func benchmarkstrSprintf(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		strSprintf(i)
	}
}

// benchmarkstrBuilder 用于测试 strBuilder
func benchmarkstrBuilder(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		strBuilder(i)
	}
}

// benchmarkstrBuffer 用于测试 strBuffer
func benchmarkstrBuffer(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		strBuffer(i)
	}
}

// benchmarkstrByte 用于测试 strByte
func benchmarkstrByte(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		strByte(i)
	}
}

// Benchmark 测试 strAdd
func BenchmarkStrAdd(b *testing.B) { benchmarkstrAdd(1000, b) }

// Benchmark 测试 strSprintf
func BenchmarkStrSprintf(b *testing.B) { benchmarkstrSprintf(1000, b) }

// Benchmark 测试 strBuilder
func BenchmarkStrBuilder(b *testing.B) { benchmarkstrBuilder(1000, b) }

// Benchmark 测试 strBuffer
func BenchmarkStrBuffer(b *testing.B) { benchmarkstrBuffer(1000, b) }

// Benchmark 测试 strByte
func BenchmarkStrByte(b *testing.B) { benchmarkstrByte(1000, b) }
