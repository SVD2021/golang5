package main

import (
	"fmt"
	"sync"
)

type Mp struct {
	mA map[string]string
	sync.RWMutex
}

func (m *Mp) Add(i string, n string) {
	m.Lock()
	m.mA[i] = n
	m.Unlock()
}

func (m *Mp) Query(n string) map[string]string {
	m.RLock()
	for i, v := range m.mA {
		if n == v {
			delete(m.mA, i)
			fmt.Printf("value matc = %s delete map=[%s]%s\n", v, i, n)
		}
	}
	m.RUnlock()
	return m.mA
}

func main() {

	m := Mp{
		mA: make(map[string]string),
	}

	go func() {
		for i := 'A'; i <= 'Z'; i++ {
			m.Add(string(i), string(i))
		}
	}()

	res := make(map[string]string)
	go func() {
		for i := 'A'; i <= 'G'; i++ {
			res = m.Query(string(i))
		}
	}()
	fmt.Scanf("\n")
	fmt.Println("剩余 map = ", res)

}
// 可以考虑下实现初始化