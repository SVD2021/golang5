package main

import (
	"fmt"
	"log"
)

type Release interface {
	update()
	init()
}

type K8s struct {
	pod map[int]string
}

func (k *K8s) init() {
	k.pod = make(map[int]string)
	k.pod[1] = "a"
	k.pod[2] = "b"
	k.pod[3] = "c"
	for i, v := range k.pod {
		log.Printf("work = %d pod = %s\n", i, v)
	}
}

func (k *K8s) update() {
	k.pod[4] = "d"
	fmt.Printf("stop pod  = %s\nstart pod = %s\n", k.pod[1], k.pod[len(k.pod)])
	for i := 1; i < 4; i++ {
		k.pod[i] = k.pod[i+1]
		log.Printf("work = %d pod = %s\n", i, k.pod[i])
	}
}

type Node struct {
	node map[int]string
}

func (n *Node) init() {
	n.node = make(map[int]string)
	n.node[1] = "a"
	n.node[2] = "b"
	n.node[3] = "c"
	for i, v := range n.node {
		log.Printf("work = %d node = %s\n", i, v)
	}
}

func (n *Node) update() {
	n.node[4] = "d"
	fmt.Printf("stop node  = %s\nstart node= %s\n", n.node[1], n.node[len(n.node)])
	for i := 1; i < 4; i++ {
		n.node[i] = n.node[i+1]
		log.Printf("work = %d node = %s\n", i, n.node[i])
	}
}

func main() {
	k := K8s{}
	n := Node{}
	loop := make(map[string]Release)
	loop["pod"] = &k
	loop["node"] = &n

	for _, v := range loop {
		v.init()
		v.update()
	}
}

// 考虑的方向是对的