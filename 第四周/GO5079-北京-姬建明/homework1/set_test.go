package homework1

import (
	"fmt"
	"testing"
)

func SetOfMap(data *[]string) []string {
	hashSet := make(map[string]string)
	set := make([]string, 0)
	for _, v := range *data {
		hashSet[v] = ""
	}
	for k, _ := range hashSet {
		// fmt.Println(k)
		set = append(set, k)
	}
	return set
}

func TestSet(t *testing.T) {

	data := []string{"a", "b", "b", "b", "c", "d", "d", "a"}
	set := SetOfMap(&data)
	fmt.Printf("转换前：%v,转换后：%v\n", data, set)

}
