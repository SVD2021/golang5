package main

import (
	"fmt"
	"log"
	"sync"
)

//简单作业

//map本身的值就是唯一的，所以去重的意思是？
//我对去重的理解是
//比如给一个带重复数据的数组，用 map 做缓存，把它去重后的数据写入一个新数组

//定义一个带读写锁的结构体
type homeworkMap struct {
	sync.RWMutex
	mp map[string]string
}

//实现 Set 方法 修改元素
func (h *homeworkMap) Set(key string, value string) {
	//写锁
	h.Lock()
	//解锁
	defer h.Unlock()
	h.mp[key] = value
}

//实现 Get 方法 查找元素
func (h *homeworkMap) Get(key string) string {
	//读锁
	h.RLock()
	//解锁
	defer h.RUnlock()
	return h.mp[key]
}

func main() {
	//定义一个重复数组
	names := [...]string{
		"tom", "tom", "jerry", "jerry", "tony", "tony", "kavin", "kavin", "啊", "啊",
	}

	//定义一个新的数组用来接收去重后的数据
	newNames := make([]string, 0)

	//定义一个 map 用做缓存
	Cache := homeworkMap{
		mp: make(map[string]string),
	}

	//去重，并且把它去重后的数据写入一个新数组
	for i := 0; i < len(names); i++ {
		if names[i] == Cache.Get(names[i]) {
			log.Printf("元素[%s]已经存在Cache中，跳过 \n", names[i])
		} else {
			//将 Cache 中不存在的值，存入Cache，并加入去重后的数组
			Cache.Set(names[i], names[i])
			newNames = append(newNames, names[i])
		}
	}

	fmt.Println("原数组为:", names)
	fmt.Println("去重后数组为:", newNames)

}
// 可以考虑实现下初始化