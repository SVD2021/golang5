package cmd

import (
	"cloudstation/user"
	"fmt"

	"github.com/spf13/cobra"
)

var (
	resUsername       string
	resPassword       string
	resBuckName       string
	resBucketEndpoint string
)

var refisterCmd = &cobra.Command{
	Use:   "register",
	Short: "注册用户绑定云商信息",
	Long:  "注册用户绑定云商信息",
	RunE: func(cmd *cobra.Command, args []string) error {
		err := registerUser()
		if err != nil {
			return err
		}
		return nil
	},
}

func registerUser() error {
	switch ossProvider {
	case "aliyun", "minio":
		m, err := user.ReadData()
		if err != nil {
			return err
		}
		resUsername := resUsername
		userInfo, err := user.NewUser(resUsername, resPassword,
			resBuckName, resBucketEndpoint, AccessID, AccessKey, ossProvider)
		if err != nil {
			return err
		}
		m[userInfo.Username] = userInfo
		err = user.WriteData(m)
		fmt.Println("注册成功")
		return err

	default:
		return fmt.Errorf("unknown oss privier options [aliyun/minio]")
	}
	return nil
}

func init() {
	refisterCmd.PersistentFlags().StringVarP(&resUsername, "username", "u", "", "register Username")
	refisterCmd.PersistentFlags().StringVarP(&resPassword, "Password", "p", "", "register Password")
	refisterCmd.PersistentFlags().StringVarP(&resBuckName, "buckname", "b", "", "register BuckName")
	refisterCmd.PersistentFlags().StringVarP(&resBucketEndpoint, "endpoint", "e", "", "register BucketEndpoint")
	RootCmd.AddCommand(refisterCmd)
}
