module cloudstation

go 1.16

require (
	github.com/AlecAivazis/survey/v2 v2.2.15
	github.com/aliyun/aliyun-oss-go-sdk v2.1.9+incompatible
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/k0kubun/go-ansi v0.0.0-20180517002512-3bf9e2903213
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/minio/minio-go/v6 v6.0.57
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/schollz/progressbar/v3 v3.8.2
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
