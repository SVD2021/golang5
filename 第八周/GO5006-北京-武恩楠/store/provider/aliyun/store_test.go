package aliyun_test

import (
	"gitee.com/wuennan/cloudstation/store/provider/aliyun"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	bucketName="owncloud-ali"
	//objectKey="XXX"
	filePath="store.go"
	endpoint = "http://oss-cn-beijing.aliyuncs.com"
	ak = "LTAI5t6Ae3VEFvhKUHWPTZqN"
	sk = "xxxxxxxxxxxxxx"
)

func TestUploadFile(t *testing.T) {
	should := assert.New(t)
	uploader,err := aliyun.NewUploader(endpoint,ak,sk)
	if should.NoError(err){
		err = uploader.UploadFile(bucketName,filePath,filePath)
		should.NoError(err)
	}
}
