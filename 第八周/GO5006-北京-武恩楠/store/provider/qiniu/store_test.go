package qiniu_test

import (
	"gitee.com/wuennan/cloudstation/store/provider/qiniu"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	bucketName="cloudstation-qiniu"
	objectKey="tsdssafdfe2ss.png"
	filePath="store.go"
	ak="0Ir4qiN6nmJzvEntGe0cC3QsVebFiwCtHYWoEKoM"
	sk="xxxxxxxxxxxxxxxxx"
	zone="ZoneHuabei"
	//sk=""
)

func TestUploadFile(t *testing.T) {
	should := assert.New(t)
	upload,err := qiniu.NewUploader(ak,sk)
	if should.NoError(err){
		err := upload.UploadFile(bucketName, objectKey, filePath)
		should.NoError(err)
	}
}

