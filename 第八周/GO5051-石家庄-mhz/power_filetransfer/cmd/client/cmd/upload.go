package cmd

import (
	"fmt"
	"path"
	"time"

	"gitee.com/infraboard/go-course/day8/cloudstation/store"
	"gitee.com/infraboard/go-course/day8/cloudstation/store/provider/aliyun"
	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
)

var (
	// BuckName todo
	defaultBuckName = ""
	defaultEndpoint = ""
	defaultALIAK    = ""
)

var (
	buckName       string
	uploadFilePath string
	bucketEndpoint string
)

// uploadCmd represents the start command
var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "上传文件到中转站",
	Long:  `上传文件到中转站`,
	RunE: func(cmd *cobra.Command, args []string) error {
		p, err := getProvider()
		if err != nil {
			return err
		}

		if uploadFilePath == "" {
			return fmt.Errorf("upload file path is missing")
		}
		day := time.Now().Format("20060102")
		fn := path.Base(uploadFilePath)
		targetFile := fmt.Sprintf("%s/%s", day, fn)
		err = p.UploadFile(buckName, targetFile, uploadFilePath)
		if err != nil {
			return err
		}
		return nil
	},
}

func getProvider() (store.Uploader, error) {

	switch ossProvider {
	case "aliyun":
		prompt := &survey.Password{
			Message: "请输入阿里云SK",
		}
		survey.AskOne(prompt, &aliAccessKey)
		return aliyun.NewUploader(bucketEndpoint, aliAccessID, aliAccessKey)
	case "qcloud":
		return nil, fmt.Errorf("not impl")
	case "mimio":
		return nil, fmt.Errorf("not impl")
	default:
		return nil, fmt.Errorf("unknown uploader %s", ossProvider)
	}
}
func init() {
	uploadCmd.PersistentFlags().StringVarP(&uploadFilePath, "file_path", "f", "", "upload file path")
	uploadCmd.PersistentFlags().StringVarP(&buckName, "bucket_name", "b", defaultBuckName, "upload oss bucket name")
	uploadCmd.PersistentFlags().StringVarP(&bucketEndpoint, "bucket_endpoint", "e", defaultEndpoint, "upload oss endpoint")
	RootCmd.AddCommand(uploadCmd)
}
