package main

import (
	"fmt"

	"github.com/guozhengli/mtools"
)

func main() {
	h := mtools.GetHostName()
	ip := mtools.GetLocalIp()
	t := mtools.GetNowTimeStr()
	fmt.Println(h, ip, t)
}
