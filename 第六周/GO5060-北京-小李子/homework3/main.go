package main

import (
	"fmt"
	"homework3/glocal"
	"homework3/trace"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

var Seting = glocal.New()

func JsonMsg(c int,s string) gin.H {
	return map[string]interface{}{
		"code": c,
		"message": s,
	}
}

func traceRequest(c *gin.Context) {
	host := c.Query("host")
	IsHttps := c.Query("is_https")
	if host == "" || IsHttps == ""{
		c.JSON(http.StatusOK, JsonMsg(100501, "args host or is_https not found"))
		return
	}
	https, err := strconv.Atoi(IsHttps)
	if err != nil {
		c.JSON(http.StatusOK, JsonMsg(100501, "args is_https not a plastic"))
		return
	}
	if https == 1 {
		t := trace.TimeGet(fmt.Sprintf("https://%s", host), Seting.HttpTrace)
		c.JSON(200, gin.H{
			"status_code": 0,
			"errMsg": "OK",
			"trace": t,
		})
		return
	}
	t := trace.TimeGet(fmt.Sprintf("https://%s", host), Seting.HttpTrace)
	c.JSON(200, gin.H{
		"status_code": 0,
		"errMsg": "OK",
		"trace": t,
	})
}

func main() {
	r := gin.Default()
	r.GET("/probe/http", traceRequest)
	r.Run(fmt.Sprintf("%s:%d",Seting.HttpIp, Seting.HttpPort) )

}
