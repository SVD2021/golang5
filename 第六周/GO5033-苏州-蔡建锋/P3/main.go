package main

/**
 * @Classname main
 * @Description TODO
 * @author cjf
 * @Date 2021/7/19 22:48
 * @Version V1.0
 */

import (
	"flag"
	"log"
	"shp/config"
	"shp/http"
)

var (
	configFile string
)

func main() {
	// 传入配置文件路径
	flag.StringVar(&configFile, "c", "simple_http_probe.yml", "config file path")
	// 解析yaml
	conf, err := config.LoadFile(configFile)
	if err != nil {
		log.Printf("[config.Load.error][err:%v]", err)
		return
	}
	log.Printf("配置是：%v", conf)
	// 启动gin
	go http.StartGin(conf)
	select {}
}
