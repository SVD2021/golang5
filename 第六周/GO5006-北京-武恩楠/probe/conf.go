package probe

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"time"
)

type Client struct {
	Host	string	`yaml:"host"`
	Timeout	time.Duration	`yaml:"timeout"`
}


func GetConf(filename string) (host string,timeout time.Duration){

	content, err := ioutil.ReadFile(filename)
	if err!=nil{
		log.Println(err)
		return
	}

	c := &Client{}
	err = yaml.UnmarshalStrict(content, c)
	if err!=nil{
		log.Println(err)
		return
	}
	host = c.Host
	timeout=c.Timeout
	return host,timeout
}

//func main()  {
//	work, timeout := yamlWork()
//	fmt.Println(work)
//	fmt.Println(timeout)
//}