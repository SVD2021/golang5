package main

import (
	"fmt"
	"simple-http-probe/probe"
)


func main() {
	// 获取探测主机及超时时间
	confFile:="conf/client.yaml"
	host, timeout := probe.GetConf(confFile)

	// 探测h
	httpProbe := probe.HttpProbe(host, timeout)
	for k,v := range httpProbe{
		fmt.Println(k,v)
	}
}
