package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	HttpServerListen string `yaml:"http_server_listen"`
	HttpProbeTimeout int    `yaml:"http_probe_timeout"`
}

func ParseConfig() (*Config, error) {
	content, err := ioutil.ReadFile("conf.yaml")
	if err != nil {
		return nil, err
	}
	var Config Config
	err = yaml.Unmarshal(content, &Config)
	if err != nil {
		return nil, err
	}
	return &Config, nil
}
