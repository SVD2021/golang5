package http

import (
	"fmt"
	"net/http"

	"simplehttpprobe/probe"

	"github.com/gin-gonic/gin"
)

func Probe(c *gin.Context) {
	host := c.Query("host")
	is_http := c.Query("is_http")

	if host == "" {
		c.String(http.StatusBadRequest, "host 参数是空的")
	}
	schema := "http"
	if is_http == "2" || is_http == "https" {
		schema = "https"
	}
	url := fmt.Sprintf("%s://%s", schema, host)
	fmt.Println(url)
	res := probe.Do_probe(url)
	c.String(http.StatusOK, res)

}
