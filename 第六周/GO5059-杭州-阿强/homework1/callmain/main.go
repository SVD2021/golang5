package main

import (
	"fmt"
	"os"

	commontools "github.com/jiqiangccie/commontools/v2"
)

func main() {
	file, err := os.Open("script.sh")
	defer file.Close()
	if err != nil {
		fmt.Println(err)
	}
	output := commontools.LinuxActioncommand(file)

	fmt.Println(output.String())
	hostname := commontools.Hostname()
	fmt.Println(hostname)
}
