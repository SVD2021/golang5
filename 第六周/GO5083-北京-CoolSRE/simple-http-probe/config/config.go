package config

import (
	"github.com/go-yaml/yaml"
	"io/ioutil"
	"log"
)

var GlobalTwSec int

type Config struct {
	HttpListenAddr         string `yaml:"http_listen_addr"`
	HttpProbeTimeoutSecond int    `yaml:"http_probe_timeout_second"`
}

func Load(in []byte) (*Config, error) {
	cfg := &Config{}
	err := yaml.Unmarshal(in, cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func LoadFile(fn string) (*Config, error) {
	content, err := ioutil.ReadFile(fn)
	if err != nil {
		return nil, err
	}
	cfg, err := Load(content)
	if err != nil {
		log.Printf("load.yaml.error:%v", err)
		return nil, err
	}
	if cfg.HttpProbeTimeoutSecond == 0 {
		GlobalTwSec = 5
	} else {
		GlobalTwSec = cfg.HttpProbeTimeoutSecond
	}
	return cfg, nil

}
