package http

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"simple-http-probe/config"
)

func StartGin(c *config.Config) {
	r := gin.Default()
	Routes(r)
	err := r.Run(c.HttpListenAddr)
	if err != nil {
		return
	}
}

func Routes(r *gin.Engine) {
	api := r.Group("/api")
	api.GET("/probe/http", HttpProbe)
	api.GET("/v1", func(c *gin.Context){
		c.String(http.StatusOK, "你好我是 http probe")
	})
}