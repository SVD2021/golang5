package http

import (
	"fmt"
	"net/http"

	"simple-http-probe/probe"
	"github.com/gin-gonic/gin"
)

func HttpProbe(c *gin.Context) {
	// 解析传过来的参数
	host := c.Query("host")
	is_https := c.DefaultQuery("is_https", "0")
	// 校验入参
	if host == "" {
		c.String(http.StatusBadRequest, "host is empty")
		return
	}
	schema := "http"
	if is_https == "1" {
		schema = "https"
	}
	url := fmt.Sprintf("%s://%s", schema, host)
	rp := probe.DoHttpProbe(url)
	c.String(http.StatusOK, rp)

}
